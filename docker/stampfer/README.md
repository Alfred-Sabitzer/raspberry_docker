# https://www.herbert.stampfer.slainte.at/

Wordpress-Konfiguration für die Homepage von Herbert Stampfer.

````bash
docker compose up -d
docker compose down
````
Mit den beiden Commandos kann man die Applikation starten bzw. stoppen.

Der Administrative Zugang ist auf https://www.herbert.stampfer.slainte.at/wp-admin
