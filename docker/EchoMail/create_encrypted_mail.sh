#!/bin/bash
#
# Generate Encrypted mail with protected Header
#  ./create_encrypted_mail.sh "${xdir}/" ${MAIL_USER} ${MAIL_RECIPIENT} ${EML_OUT}/mail_${COUNTER}.draft
#
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#

workdir=$1
MAIL_USER=$2
MAIL_RECIPIENT=$3
MAIL_FILE=$4

message_id=$($(dirname "$0")/scripts/messageid.sh)
maildate=$($(dirname "$0")/scripts/date.sh)
cat <<EOF > ${workdir}/header.txt
MIME-Version: 1.0
User-Agent: ${0}
Content-Language: en-US, de-AT-frami
From: ${MAIL_USER}
To: ${MAIL_RECIPIENT}
${message_id}
${maildate}
Subject: ...
EOF

boundary_multipart_mixed="mm$($(dirname "$0")/scripts/boundary.sh)"
boundary_mixed_multipart="mp$($(dirname "$0")/scripts/boundary.sh)"

cat <<EOF > ${workdir}/multipart.txt
Content-Type: multipart/mixed; boundary="${boundary_multipart_mixed}";
 protected-headers="v1"
MIME-Version: 1.0
From: ${MAIL_USER}
To: ${MAIL_RECIPIENT}
${message_id}
${maildate}
Subject:  =?UTF-8?Q?$(cat ${workdir}/subject.txt)?=

--${boundary_multipart_mixed}
Content-Type: multipart/mixed; boundary="${boundary_mixed_multipart}"

--${boundary_mixed_multipart}
Content-Type: text/html; charset=UTF-8; format=flowed
Content-Transfer-Encoding: base64

$(base64 <${workdir}/body.html)
EOF

for FILE in ${workdir}/Attachments/*; do
  cat <<EOF > ${workdir}/multipart.part

--${boundary_mixed_multipart}
$($(dirname "$0")/scripts/file.sh "${FILE}")

$(base64 <"${FILE}")
EOF
  cat ${workdir}/multipart.part >> ${workdir}/multipart.txt
done;

cat <<EOF >> ${workdir}/multipart.txt

--${boundary_mixed_multipart}--

--${boundary_multipart_mixed}--
EOF

echo "${MAIL_SECRET_KEY_PASSWORD}" | gpg --batch --yes --passphrase-fd 0 --encrypt --sign --armor --trust-model always --recipient ${MAIL_RECIPIENT} --local-user ${MAIL_USER} --output ${workdir}/multipart.txt.pgp ${workdir}/multipart.txt

boundary_pgp="pgp$($(dirname "$0")/scripts/boundary.sh)"
cat <<EOF > ${workdir}/multipart.pgp
Content-Type: multipart/encrypted; protocol="application/pgp-encrypted";
 boundary="${boundary_pgp}"
MIME-Version: 1.0

--${boundary_pgp}
Content-Type: application/pgp-encrypted
MIME-Version: 1.0
Content-Description: PGP/MIME version identification

Version: 1

--${boundary_pgp}
Content-Type: application/octet-stream; name="encrypted.asc"
MIME-Version: 1.0
Content-Description: OpenPGP encrypted message
Content-Disposition: inline; filename="encrypted.asc"

$(cat ${workdir}/multipart.txt.pgp)

--${boundary_pgp}--
EOF

cat ${workdir}/header.txt ${workdir}/multipart.pgp > ${MAIL_FILE}
