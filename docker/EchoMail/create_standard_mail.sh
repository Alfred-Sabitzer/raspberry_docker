#!/bin/bash
#
# Generate standdard mail with protected Header
#  ./create_standard_mail.sh "${xdir}/" ${MAIL_USER} ${MAIL_RECIPIENT} ${EML_OUT}/mail_${COUNTER}.draft
#
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#

workdir=$1
MAIL_USER=$2
MAIL_RECIPIENT=$3
MAIL_FILE=$4

message_id=$($(dirname "$0")/scripts/messageid.sh)
maildate=$($(dirname "$0")/scripts/date.sh)
cat <<EOF > ${workdir}/header.txt
MIME-Version: 1.0
User-Agent: ${0}
Content-Language: en-US, de-AT-frami
From: ${MAIL_USER}
To: ${MAIL_RECIPIENT}
${message_id}
${maildate}
Subject:  $(cat ${workdir}/subject_standard.txt)
EOF

boundary_multipart_mixed=$($(dirname "$0")/scripts/boundary.sh)
boundary_multipart_alternative=$($(dirname "$0")/scripts/boundary.sh)

cat <<EOF > ${workdir}/multipart.txt
Content-Type: multipart/mixed; boundary="${boundary_multipart_mixed}"

--${boundary_multipart_mixed}
Content-Type: multipart/alternative;
 boundary="${boundary_multipart_alternative}"


--${boundary_multipart_alternative}
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: base64

$(base64 <${workdir}/body.txt)
--${boundary_multipart_alternative}
Content-Type: text/html; charset=UTF-8; format=flowed
Content-Transfer-Encoding: base64

$(base64 <${workdir}/body.html)
--${boundary_multipart_alternative}--
EOF

for FILE in ${workdir}/Attachments/*; do
  cat <<EOF > ${workdir}/multipart.part

--${boundary_multipart_mixed}
$($(dirname "$0")/scripts/file.sh "${FILE}")

$(base64 <"${FILE}")
EOF
  cat ${workdir}/multipart.part >> ${workdir}/multipart.txt
done;

cat <<EOF >> ${workdir}/multipart.txt

--${boundary_multipart_mixed}--

EOF

cat ${workdir}/header.txt ${workdir}/multipart.txt > ${MAIL_FILE}
