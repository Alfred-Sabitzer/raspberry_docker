# EchoMail

Mail Daemon - send the received email back to sender.

# Bauen

````bash
./up.sh
````

Mit diesem Kommando wird der Container gebaut und gestartet.

# Typische Antwort

Prinzipielle gibt es eine freundliche Antwort

````text
Hi

Das ist der automatische Echo-Mailer auf https://www.slainte.at .

Deine Mail ist als Attachment angefügt.

This is the automated echo-mailer on https://www.slainte.at.

Your orginal mail is attached.


Have FUN
````

