#!/bin/bash
############################################################################################
#
# Versenden der Email
#
# https://www.linuxshelltips.com/curl-send-email-linux/
# https://think.unblog.ch/en/how-to-send-mail-use-curl/
# https://www.spamresource.com/2022/04/lets-send-emailwith-curl.html
# https://everything.curl.dev/usingcurl/smtp
#
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

MAIL_FILE=$1
MAIL_RECIPIENT=$2

python ~/SendMail.py --MAIL_SMTP_HOST=${MAIL_SMTP_HOST} \
      --MAIL_USER=${MAIL_USER} \
      --MAIL_RECIPIENT=${MAIL_RECIPIENT} \
      --MAIL_PASSWORD="${MAIL_PASSWORD}" \
      --MAIL_FILE=${MAIL_FILE}

############################################################################################################################################
 ##
 ## https://support.plesk.com/hc/en-us/articles/20970581221527-Unable-to-send-emails-using-PHP-Scripts-bare-LF-received-after-DATA
 ##
 ## https://nvd.nist.gov/vuln/detail/CVE-2023-51764
 ##
 ## Sending Emails with Curl does not work anymore in combination with mailcow
 ## No Fix avalable at the moment
 ############################################################################################################################################
 #curl --ssl-reqd --verbose smtp://${MAIL_SMTP_HOST} --mail-from ${MAIL_USER} \
 #     --mail-rcpt ${MAIL_RECIPIENT} --upload-file ${MAIL_FILE} \
 #     --user "${MAIL_USER}:${MAIL_PASSWORD}"