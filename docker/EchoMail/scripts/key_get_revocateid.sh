#!/bin/bash
############################################################################################
# Lesen der Revocate-ID
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER=$1

# get key-Dates
while read line; do
  #echo "#${line}#"
  case "${line}" in
    *[*:*)
        # Not interesting
      ;;
    uid*)
        # Not interesting
      ;;
    sub*)
        # Not interesting
      ;;
    pub*)
        # Not interesting
      ;;
    *)
       if [[ -n "${line// /}" ]]; then
          revocateid=${line#     }
       fi
      ;;
  esac
done < <(gpg --list-keys ${MAIL_USER} )
echo "${revocateid}"