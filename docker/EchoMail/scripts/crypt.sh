#!/bin/bash
############################################################################################
# Demo Kommandos
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
############################################################################################
# Verschlüsseln einer Datei
############################################################################################
echo password | gpg --batch --yes --passphrase-fd 0 --encrypt --sign --armor --trust-model always --recipient echo@slainte.at --local-user alfred@slainte.at --output ./Hallo.pgp ./Hallo.txt
############################################################################################
# Erzeugen einer Detached sign Datei
############################################################################################
echo password | gpg --batch --yes --passphrase-fd 0 --detach-sign --armor --trust-model always --local-user alfred@slainte.at --output ./Hallo.pgp ./Hallo.txt
