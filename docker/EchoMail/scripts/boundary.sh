#!/bin/bash
############################################################################################
# Erzeugen einer Boundary
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# ===============1771672613201425469==
now=$(date +"%Y%m%d%H%M%S%s")
echo "==========${now}${RANDOM}=="
# ==========202405200920251716189625==