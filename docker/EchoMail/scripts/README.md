# scripts

Hier befinden sich alle Scripten.
Diese Skripten können dann in anderen Anwendungen/Repos verwendet werden.

````bash
curl https://gitlab.com/Alfred-Sabitzer/raspberry_docker/-/archive/main/raspberry_docker-main.tar.gz?path=docker/scripts/Mail/scripts --output scripts.tar
tar -xvf scripts.tar 
mv ./raspberry_docker-main-docker-scripts-Mail-scripts/docker/scripts/Mail/scripts/* ./scripts
rmdir -p ./raspberry_docker-main-docker-scripts-Mail-scripts/docker/scripts/Mail/scripts/
````

Damit sind dann alle Dateien am richtigen Ort (in der neuesten Version).