#!/bin/bash
############################################################################################
# Ausgabe der File-Attribute für Mails
# Es gibt leider keinen sinnvollen Mechanismus, um die Content-Description zu kriegen.
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
FILE_NAME=${1}
FILE_NAME_SHORT=${FILE_NAME##*/} # Alles nach dem letzten /
#
mime=$(file --brief --mime "${FILE_NAME}")
echo "Content-Type: ${mime}; name=\"${FILE_NAME_SHORT}\""
echo "Content-Disposition: attachment; filename=\"${FILE_NAME_SHORT}\""
echo "Content-Description: This is an attachment."
echo "Content-Transfer-Encoding: base64"
