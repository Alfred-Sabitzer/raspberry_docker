#!/bin/bash
############################################################################################
# Schlüssel löschen und neu erzeugen
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER=$1
keyserver="https://keyserver.ubuntu.com"
create_key () {
  cat <<EOF | gpg --batch --yes --generate-key
           %echo Generating a basic OpenPGP key
           Key-Type: RSA
           Key-Length: 4096
           Key-Usage: sign
           Subkey-Type: RSA
           Subkey-Length: 4096
           Subkey-Usage: encrypt
           Name-Real: ${MAIL_USER}
           #Name-Comment: Email for ${MAIL_USER} with no passphrase, Expiration will be in 2 Months
           Name-Email: ${MAIL_USER}
           %no-protection
           Expire-Date: 2m
           Keyserver: ${keyserver}
           # Do a commit here, so that we can later print "done" :-)
           %commit
           %echo done
EOF
}

gpg --list-key ${MAIL_USER}
retVal=$?
if [ ${retVal} -ne 0 ]; then
    echo "Key for ${MAIL_USER} not found"
    create_key
else
  revocateid=$($(dirname "$0")/key_get_revocateid.sh ${MAIL_USER} )
  echo "revocateid: ${revocateid}"
  sed -i 's/:-----BEGIN PGP PUBLIC KEY BLOCK-----/-----BEGIN PGP PUBLIC KEY BLOCK-----/' ~/.gnupg/openpgp-revocs.d/${revocateid}.rev
  gpg --import ~/.gnupg/openpgp-revocs.d/${revocateid}.rev
  gpg --keyserver ${keyserver} --send-key ${revocateid}
  gpg --batch --yes --delete-secret-key ${revocateid}
  gpg --batch --yes --delete-key ${revocateid}
  rm ~/.gnupg/openpgp-revocs.d/${revocateid}.rev
  create_key
fi
revocateid=$($(dirname "$0")/key_get_revocateid.sh ${MAIL_USER} )
gpg --keyserver ${keyserver} --send-key ${revocateid}