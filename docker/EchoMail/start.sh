#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Start Container "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# i am still root
whoami
pwd
ls -lisa
# Richtige Berechtigung für gpg
chown -R echomail:echomail /home/echomail/.gnupg/
chmod 600 /home/echomail/.gnupg/*
chmod 700 /home/echomail/.gnupg
#
cd /home/echomail/.gnupg
chown -R echomail:echomail *
echo "Change to Real user-account"
su echomail -c "/bin/bash /home/echomail/start_echomail.sh"
#
