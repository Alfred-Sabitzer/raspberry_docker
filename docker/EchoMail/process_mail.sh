#!/bin/bash
echo "############################################################################################"
echo "#"
echo "# Verarbeiten der Mails"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

ls -d -R -A -l ${EML_PARSED}/* > /tmp/dir.txt

#-rw-r--r--    1 root     root          5566 Dec 31 13:16 /EML_PARSED/mail_1.eml.pgp.eml
#drwxr-xr-x    2 root     root          4096 Dec 31 13:16 /EML_PARSED/mail_1.eml.pgp.eml_attachments

if [[ -f /tmp/mails_to_do.txt ]];
then
  COUNTER=0
  cat /tmp/dir.txt |
  while IFS=' ' read -r att size owner group byte month day time xdir
  do
    if [[ -d "${xdir}" ]];
    then
      # $f is a directory
      echo "Processing #################################################################################################### ${xdir}"
      echo "Dir: ${xdir}"
      let COUNTER++
      cd "${xdir}/"
      pwd
      # Verarbeite PGP-Keys
      for f in ./OpenPGP*.asc; do
        echo "Processing $f file...";
        gpg --import --verbose ${f}
      done

      # Aufräumen
      header=$(cat header.txt)
      rm *
      mkdir -p ./Attachments
      # Standard Mail Komponenten
      cp --verbose ~/mail/body.* .
      cp --verbose ~/mail/subject*.txt .
      cp --verbose ~/mail/Attachments/* ./Attachments/.
      # Exchange Sender and Recipient
      echo "${header}" > header.txt
      sed -i "s_To: _Toxxxxxx: _" header.txt
      sed -i "s_From: _To: _" header.txt
      sed -i "s_Toxxxxxx: _From: _" header.txt
      #cp ${EML_IN}/mail_${COUNTER}.eml ./Attachments/mail_${COUNTER}.txt
      # Als EML und Text gibt es Darstellungsprobleme im Thunderbird (versucht das File zu interpretieren, und kann es dann nicht verarbeiten)
      tar -C ${EML_IN} -cvzf ./Attachments/mail_${COUNTER}.tar.gz mail_${COUNTER}.eml
      # Get Key ID
      key_id_short=$(~/scripts/key_get_revocateid.sh ${MAIL_USER})
      # Add public key
      gpg --export --armor --batch --yes --output "./Attachments/OpenPGP_0x${key_id_short}.asc" ${MAIL_USER}
      # determine recipient
      MAIL_RECIPIENT=$(cat header.txt | grep -i To: )
      MAIL_RECIPIENT=${MAIL_RECIPIENT#*<}
      MAIL_RECIPIENT=${MAIL_RECIPIENT%>*}
      echo "Recipient ${MAIL_RECIPIENT}"
      gpg --list-key ${MAIL_RECIPIENT}
      retVal=$?
      if [ ${retVal} -eq 0 ]; then
          echo "Create Encrypted Mail for ${MAIL_USER} and ${MAIL_RECIPIENT}"
          ~/create_encrypted_mail.sh "${xdir}/" ${MAIL_USER} ${MAIL_RECIPIENT} ${EML_OUT}/mail_${COUNTER}.draft
      else
          echo "Create Standard Mail for ${MAIL_USER} and ${MAIL_RECIPIENT}"
          ~/create_standard_mail.sh "${xdir}/" ${MAIL_USER} ${MAIL_RECIPIENT} ${EML_OUT}/mail_${COUNTER}.draft
      fi
      # Process Mail
      muacrypt process-outgoing < ${EML_OUT}/mail_${COUNTER}.draft > ${EML_OUT}/mail_${COUNTER}.eml
      ~/send_mail.sh  ${EML_OUT}/mail_${COUNTER}.eml ${MAIL_RECIPIENT}
      echo "Processing End #################################################################################################### ${xdir}"
    fi
  done
fi
