# EchoMail - Test

Skripten um die Mail-Funktionalität zu testen

````bash
./export_keys.sh 
./test_create_standard_mail.sh 
./test_create_encrypted_mail.sh 
````

export_keys.sh export den gpg-key. Der kommt dann in das Attachment Verzeichnis.
test_create_standard_mail.sh erzeugt eine normale Mail.
test_create_encrypted_mail.sh erzeugt eine verschlüsselte Mail. 



