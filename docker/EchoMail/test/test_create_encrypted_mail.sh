#!/bin/bash
#
# Testen der Mail-Funktionalität
#  ./create_encrypted_mail.sh "${xdir}/" ${MAIL_USER} ${MAIL_RECIPIENT} ${EML_OUT}/mail_${COUNTER}.draft
#
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
export MAIL_SECRET_KEY_PASSWORD=""
../create_encrypted_mail.sh "$(dirname "$0")/create_encrypted_mail" test@slainte.at alfred@slainte.at $(dirname "$0")/create_encrypted_mail/create_encrypted_mail.eml
#