#!/bin/bash
#
# Bereitstellen des gpg-keys
#
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Get Key ID
key_id_short=$($(dirname "$0")./scripts/key_get_revocateid.sh alfred@slainte.at)
# Add public key
gpg --export --armor --batch --yes --output "OpenPGP_0x${key_id_short}.asc" alfred@slainte.at