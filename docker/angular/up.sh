#!/bin/bash
# Build the image and start it.
image="angular"
docker build . -t ${image}:latest
docker compose up -d
docker export "$(docker ps | grep -i ${image}:latest | grep -i ${image} | awk '{print $1 }')" | gzip >  ${image}_latest.tar.gz
