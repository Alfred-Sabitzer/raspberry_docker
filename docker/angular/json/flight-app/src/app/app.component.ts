import { Component } from '@angular/core';
import { HomeComponent } from './home/home.component';

@Component({
  standalone: true,
  imports: [
    HomeComponent,
  ],
  selector: 'app-root',
/*  template: `<h1>Hello world!</h1>`,*/
  template: `
    <main>
      <header class="brand-name">
        <img class="brand-logo" src="/assets/logo.svg" alt="logo" aria-hidden="true">
      </header>
      <section class="content">
        <app-home></app-home>
      </section>
    </main>
  `,
/*  templateUrl: './app.component.html', */
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'homes';
}

