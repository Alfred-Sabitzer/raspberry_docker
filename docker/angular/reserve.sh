
FROM nginx:mainline-alpine

COPY --from=builder /my-app /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

LABEL maintainer="slainte@slainte.at"
LABEL Description="Angular Frontend."

# timezone support
ENV TZ=Europe/Vienna
RUN apk add --update tzdata --no-cache &&\
    cp /usr/share/zoneinfo/${TZ} /etc/localtime &&\
    echo $TZ > /etc/timezone \

# Update
RUN apk upgrade --update-cache --available
RUN rm -rf /var/cache/apk/*

# Load Shellscripts
COPY *.sh /
RUN chmod 755 /*.sh

#CMD ["/start.sh"]
# Configure a healthcheck to validate that everything is up&running
#HEALTHCHECK --interval=60s --timeout=10s CMD curl -k --silent --fail https://127.0.0.1/ping || exit 1
