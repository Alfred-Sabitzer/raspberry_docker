# Angular

Install angular framework and create frontend.

## Setup
https://angular.io/guide/setup-local

```shell
npm install -g @angular/cli
```

## Application

* https://www.angulararchitects.io/blog/angular-tutorial-teil-1-werkzeuge-und-erste-schritte/
* https://angular.io/start

Diese Links erklären die Installation und die ersten Schritte.

```shell
ng new flight-app
cd flight-app
ng serve -o
```
Hier wird eine erste Applikation erzeugt und gestartet.

### Application builden und deployen

```shell
ng add @angular-architects/paper-design
```
Für den "Bootstrap" (Design für look and Feel) installieren wir das freie Paper Design. 

```shell
ng build
```

Mit diesem Kommando wird die Applikation gebaut.

## Hello World

https://angular.io/tutorial/first-app/first-app-lesson-01

Dieser Link führt einen recht gut durch die Prinzipien von Angular.

## CMS

https://strapi.io/

Es gibt natürlich Sinn nicht alles selbst zu machen, sondern ein CMS dafür zu verwenden.
Ein recht beliebtes ist offensichtlich strapi.

## IDE

### Webstorm

https://de.wikipedia.org/wiki/WebStorm

WebStorm ist eine integrierte Entwicklungsumgebung (IDE) der Firma JetBrains für die Programmiersprache JavaScript. Darüber hinaus unterstützt WebStorm HTML5, Node.js, Bootstrap, Angular/AngularJS, TypeScript, PhoneGap/Cordova, Dart und viele weitere Techniken, und wird daher vorwiegend zur Entwicklung von webbasierten Mobile Apps eingesetzt. WebStorm basiert auf der IntelliJ IDEA der Firma JetBrains, stellt jedoch die auf JavaScript-spezialisierte Version dar.

Leider ist das kostenpflichtig.

### Sublime Text

https://www.sublimetext.com/

Sublime Text is the best Angular code Editor around; compact, reliable, and extremely quick. The capacity of Sublime Text to handle extensive documents without experiencing any issues is among its most praised qualities. Moreover, it has excellent results, mainly its capacity to scan SQL statements and case-sensitive elements.

