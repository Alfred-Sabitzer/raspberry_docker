#!/bin/bash
# Setup Angular
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

git config --global user.email "slainte@slainte.at"
git config --global user.name "Slainte Master User"

npm install -g @angular/cli
ng new my-app << EOF
Y
Y
CSS
EOF
cd /my-app
ng build --configuration production
