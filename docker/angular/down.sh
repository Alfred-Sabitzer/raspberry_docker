#!/bin/bash
# Stop container, remove image
image="angular"
docker kill ${image}
docker rm --force ${image}
docker rmi ${image}
