#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Start Container "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

#
while [ true ]
do
	echo "`date` [`hostname`]"
	sleep 60;
done
#
