# Nginx Reverse Proxy Manager

Siehe https://nginxproxymanager.com/guide/#project-goal
bzw. https://hub.docker.com/r/jc21/nginx-proxy-manager

Einschalten zusätzlicher Firewall-Regeln, damit die anderen "internen" Netze über den Reverse Proxy erreichbar sind.

````bash
sudo ufw allow from 127.0.0.0/8; # vom eigenen Host
sudo ufw allow from 10.0.0.0/24; # Internes IP-Netz
sudo ufw allow from 172.16.0.0/24; # Internes IP-Netz
sudo ufw allow from 192.168.0.0/16;  # Erreichbar aus dem lokalen Netz
````

