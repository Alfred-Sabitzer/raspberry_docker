#!/bin/bash
# Build the image and start it.
image="nginxproxy"
docker build --no-cache . -t ${image}:latest
docker compose up -d
docker export "$(docker ps | grep -i ${image}:latest | grep -i nginxproxy | awk '{print $1 }')" | gzip >  ${image}_latest.tar.gz
