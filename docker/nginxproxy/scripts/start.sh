#!/bin/sh
############################################################################################
#
# Prepare container
#
############################################################################################

# Create symbolic links
cd /etc/nginx/sites-enabled/
for file in /etc/nginx/sites-available/*.conf
do
  # commands to execute for each file
  #echo "Processing file: $file"
  ln -s ${file} /etc/nginx/sites-enabled/
done

# Start cron
rc-service crond start && rc-update add crond

# Grant Rights
chown www-data:www-data -R /var/www/*
chmod 755 -R /var/www/*
# Starten des Supervisors
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
