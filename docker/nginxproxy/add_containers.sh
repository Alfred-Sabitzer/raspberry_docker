#!/bin/bash
# Hinzufügen der Container zum Netzwerk
shopt -o -s errexit #—Terminates  the shell script if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
IFS="
"
docker network connect proxynet portainer