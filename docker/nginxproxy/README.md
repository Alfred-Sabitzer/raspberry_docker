# Nginx Reverse Proxy Manager

Diese ngxinx-Configuration stellt einen Reverse-Proxy mit Admin-Page bereit.
Der Proxy ist selbst unter einen Domain bereitt.

## Vorarbeiten

````sh
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --force-renewal --email slainte@slainte.at -d unzmarkt.slainte.at -d www.unzmarkt.slainte.at
````

Die Domain muß als Environment Variable vorhanden sein.
Im Start wird das geprüft. und ggf. angelegt.
Bedienung so einfach wie möglich.



## Dokumentadion reverse Proxy

Die nachfolgneden Links verweisen auf Beispiele für Reverse-Proxies.


* https://nginx.org/en/docs/http/ngx_http_proxy_module.html#example
* https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
* http://nginx.org/en/docs/http/ngx_http_realip_module.html

Interessant ist natürlich die echte client-ip. Dazu braucht es aber offensichtlich ein Modul.

## Dokumentation der nginx-Module

Hier wird auf die Dokumentation der Module verwiesen.

* https://nginx.org/en/docs/
* http://nginx.org/en/docs/http/ngx_http_realip_module.html

Interessant ist die Konfiguration des http_realip_module.
