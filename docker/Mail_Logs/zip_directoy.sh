#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Zippen eines Input-Directories "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
filemask="$1"
outfile="$2"
#tar --file=ARCHIV --create --verbose --gzip --exclude-caches --ignore-command-error  "${outfile}" ${filemask}
#BusyBox v1.36.1 (2024-06-10 07:11:47 UTC) multi-call binary.
#Usage: tar c|x|t [-ZzJjahmvokO] [-f TARFILE] [-C DIR] [-T FILE] [-X FILE] [LONGOPT]... [FILE]...
#Create, extract, or list files from a tar file
#\00\00\00\00\00\00
#e
#	x	Extract
#	t	List
#	-f FILE	Name of TARFILE ('-' for stdin/out)
#	-C DIR	Change to DIR before operation
#	-v	Verbose
#	-O	Extract to stdout
#	-m	Don't restore mtime
#	-o	Don't restore user:group
#	-k	Don't replace existing files
#	-Z	(De)compress using compress
#	-z	(De)compress using gzip
#	-J	(De)compress using xz
#	-j	(De)compress using bzip2
#	--lzma	(De)compress using lzma
#	-a	(De)compress based on extension
#	-h	Follow symlinks
#	-T FILE	File with names to include
#	-X FILE	File with glob patterns to exclude
#	--exclude PATTERN	Glob pattern to exclude
#	--overwrite		Replace existing files
#	--strip-components NUM	NUM of leading components to strip
#	--no-recursion		Don't descend in directories
#	--numeric-owner		Use numeric user:group
#	--no-same-permissions	Don't restore access permissions
tar -cvzf "${outfile}" ${filemask}
#