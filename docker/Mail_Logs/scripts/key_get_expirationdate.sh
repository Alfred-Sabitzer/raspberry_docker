#!/bin/bash
############################################################################################
# Lesen des Expiration-Dates
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER=$1
expirationdate="never"
# get key-Dates
while read line; do
  #echo "#${line}#"
  case "${line}" in
    *[*:*)
        expirationdate=${line#*[*:}
        expirationdate=${expirationdate%]*}
      ;;
    uid*)
        # Not interesting
      ;;
    *)
        # Not interesting
      ;;
  esac
done < <(gpg --list-keys ${MAIL_USER} )
echo "${expirationdate}"