#!/bin/bash
############################################################################################
# Lesen des kurzen Key-ID
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER=${1}
echo "$(gpg --list-keys --keyid-format=long ${MAIL_USER} | grep -i pub | grep -o -P '(?<=/)[A-Z0-9]{16}')"
### Achtung: Unter alpkne geht -P nicht