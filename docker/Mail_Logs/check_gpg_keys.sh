#!/bin/bash
############################################################################################
# Prüfen Ob Keys Vorahnden und gültig
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER=$1
gpg --list-key ${MAIL_USER}
retVal=$?
if [ ${retVal} -ne 0 ]; then
    echo "Key for ${MAIL_USER} not found"
    $(dirname "$0")/scripts/create_keys.sh ${MAIL_USER}
else
  ed=$($(dirname "$0")/scripts/key_get_expirationdate.sh ${MAIL_USER})
  #echo "#${ed}#"
  # 2024-07-19
  now=$(date +"%Y-%m-%d")
  if [[ "#${ed}#" < "#${now}#" ]]; then
    $(dirname "$0")/scripts/create_keys.sh ${MAIL_USER}
  fi
fi