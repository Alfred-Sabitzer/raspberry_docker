#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Start Container "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
workdir=$(mktemp -d)
mkdir -p ${workdir}/Attachments
while [ true ]
do
  cd ~
	echo "`date` [`hostname`]"
# gpg key management
  mkdir -p ~/.gnupg/
  chown -R $(whoami) ~/.gnupg/
  chmod 600 ~/.gnupg/*
  chmod 700 ~/.gnupg
  chmod 700 /home/maillog/.gnupg
  chmod 700 /home/maillog/.gnupg/crls.d
  chmod 700 /home/maillog/.gnupg/openpgp-revocs.d
  chmod 700 /home/maillog/.gnupg/private-keys-v1.d
  ./check_gpg_keys.sh ${MAIL_USER}
# Import trusted public keys
	gpg --import --verbose ./mail_logs/*.gpg
#	prepare directory
	rm -f ${workdir}/*
	rm -f ${workdir}/Attachments/*
	cp ./mail_logs/body.html ${workdir}/
#	Prepare Content
	datum=$(date +"%Y-%m-%d %T")
	dateien=$(ls -lisa ${FILES_TO_SEND})
	sed -i "s/#Datum#/${datum}/g" ${workdir}/body.html
	sed -i "s,#Dateien#,${FILES_TO_SEND},g" ${workdir}/body.html
	echo "Files aus "${FILES_TO_SEND}" vom ${datum}" > ${workdir}/subject.txt
#	Zip files
  ls -lisa ${FILES_TO_SEND} > "${workdir}/Attachments/ls.txt"
  ./zip_directoy.sh "${FILES_TO_SEND}" "${workdir}/Attachments/var_log.tar.gz"
  key_id_short=$(./scripts/key_get_revocateid.sh ${MAIL_USER})
# Add public key
  gpg --export --armor --batch --yes --output "${workdir}/Attachments/OpenPGP_0x${key_id_short}.asc" ${MAIL_USER}
# ceare eml
  ./create_mail.sh ${workdir} ${MAIL_USER} ${MAIL_RECIPIENT}
  ./send_mail.sh ${workdir}/mail.eml
# Sleep
	sleep ${MAIL_INTERVAL_SECONDS};
done
#
