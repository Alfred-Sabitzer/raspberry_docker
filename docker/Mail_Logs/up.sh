#!/bin/bash
# Build the image and start it.
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

image="maillog"
tag=$(date '+%Y%m%d')
#
rm -f ./scripts/*
curl https://gitlab.com/Alfred-Sabitzer/raspberry_docker/-/archive/main/raspberry_docker-main.tar.gz?path=docker/scripts/Mail/scripts --output scripts.tar
tar -xvf scripts.tar
mv ./raspberry_docker-main-docker-scripts-Mail-scripts/docker/scripts/Mail/scripts/* ./scripts
rmdir -p raspberry_docker-main-docker-scripts-Mail-scripts/docker/scripts/Mail/scripts/
rm -f ./scripts.tar
#
sudo chown -R 10001:10001 .gnupg
docker build --no-cache --pull --force-rm --tag ${image}:${tag}  . -f dockerfile
docker kill ${image}
docker container rm --force ${image}
#sudo chown -R 10001:10001 ./.gnupg
docker run --detach \
  -v /var/log:/myvar/log \
  --env-file ./maillog.config \
  --name ${image} \
  --restart always ${image}:${tag}
docker ps
docker export "$(docker ps | grep -i ${image}:${tag} | grep -i ${image} | awk '{print $1 }')" | gzip >  ${image}_${tag}.tar.gz
