#!/bin/bash
# Stopeen des Portainers
shopt -o -s errexit #—Terminates  the shell script if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
IFS="
"
#docker ps
#docker stop portainer
#docker stop portainer_agent
#docker rm portainer
#docker rm portainer_agent
#docker ps

docker-compose down