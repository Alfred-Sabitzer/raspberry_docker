#!/bin/bash
# Installieren des Portainers
#shopt -o -s errexit #—Terminates  the shell script if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#IFS="
#"
sudo lsof -i -P -n | grep LISTEN | grep -i 9443
docker ps
image="portainer"
docker kill ${image}
docker container rm --force ${image}
#docker-compose up -d
docker ps
docker run -d -p 127.0.0.1:8000:8000 -p 127.0.0.1:9443:9443 --name portainer \
  --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock:ro  \
  -v /etc/localtime:/etc/localtime:ro \
  -v ./portainer-data:/data \
  --network="proxynet" \
  portainer/portainer-ce:latest
docker ps
#docker run -d -p 127.0.0.1:9001:9001 --name portainer_agent \
#  --restart=always \
#  -v /var/run/docker.sock:/var/run/docker.sock \
#  -v /var/lib/docker/volumes:/var/lib/docker/volumes \
#  --network="proxynet" \
#  portainer/agent:latest
