#!/bin/bash
# https://www.digitalocean.com/community/tutorials/how-to-use-certbot-standalone-mode-to-retrieve-let-s-encrypt-ssl-certificates-on-ubuntu-16-04
# Erzeugen der Zertifikate
#
# sudo certbot certificates
# sudo certbot delete --cert-name example.com
#
# wwwq, cdn, mail
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --force-renewal --email slainte@slainte.at -d unzmarkt.slainte.at -d www.unzmarkt.slainte.at
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --force-renewal --email slainte@slainte.at -d slainte.at -d www.slainte.at -d cdn.slainte.at
# no wwww, no cdn
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --email slainte@slainte.at -d test.slainte.at
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --email slainte@slainte.at -d mailcow.slainte.at
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --email slainte@slainte.at -d nextcloud.slainte.at
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --email slainte@slainte.at -d portainer.slainte.at
sudo certbot certonly --standalone --preferred-challenges http --rsa-key-size 4096 --non-interactive --agree-tos --no-eff-email --force-renewal --email slainte@slainte.at -d unzmarkt.duckdns.org
