# Certbot

Hier wird der Certbot installiert und die Zertifikate erzeugt.

Die Grundinstallation folgt https://www.digitalocean.com/community/tutorials/how-to-use-certbot-standalone-mode-to-retrieve-let-s-encrypt-ssl-certificates-on-ubuntu-16-04

Der Eintrag im Cron für das Erneuern der Zertifikate ist

```cron
0 0,12 * * * sleep 57 && certbot renew -q
```

# Vorbereitung

Nachdem das Erzeugung der Zertifikate mit einem eigenen Webserver efolgt ("Standalone") muß man zuerst den Reverse-Proxy stoppen und dann wieder starten.
Das ist eber eh ganz gut (dann werden die aktuelleste Images geholt).
