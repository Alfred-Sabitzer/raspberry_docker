#!/bin/bash
chmod 755 /var/www/*
php-fpm --daemonize --fpm-config /usr/local/etc/php/fpm-pool.conf &
nginx -g 'daemon off;'