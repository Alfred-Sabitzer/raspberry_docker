# Joonla - Apline

Die Installation biasiert auf https://github.com/damalis/full-stack-proxy-nginx-joomla-for-everyone-with-docker-compose/tree/main
Adaptionen sind 

- Ich habe schon einen Proxy
- Ich brauche keinen Certbot (macht der Proxy)
- Mailhog ist nicht notwendig
- phpmyadmin ist nicht notwendig

Als Image wird immer alpine genommen (soweit möglich).

Um Overcommit zu ermöglichen muß die Systemkonfig erweitert werden.

```bash
sudo echo 'vm.overcommit_memory = 1' | sudo tee /etc/sysctl.conf
sudo shutdown -r now 
```



