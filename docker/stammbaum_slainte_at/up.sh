#!/bin/bash
docker build --no-cache . -t stammbaumslainteat:latest
docker compose up -d
docker export "$(docker ps | grep -i stammbaumslainteat:latest| grep -i stammbaumslainteat | awk '{print $1 }')" | gzip >  stammbaumslainteat_latest.tar.gz