#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Start Container "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Richtige Berechtigung für gpg
chown -R $(whoami) ~/.gnupg/
chmod 600 ~/.gnupg/*
chmod 700 ~/.gnupg
#
while [ true ]
do
	echo "`date` [`hostname`]"
  # Create Directories
  mkdir ${EML_IN}
  chmod 777 ${EML_IN}
  mkdir ${EML_OUT}
  chmod 777 ${EML_OUT}
  mkdir ${EML_PARSED}
  chmod 777 ${EML_PARSED}
  ./check_gpg_keys.sh
  ./read_mail.sh
  ./process_mail.sh
  ./send_mail.sh
  ./delete_mail.sh
  #Clean Up Directories
  rm -rf ${EML_IN}
  rm -rf ${EML_OUT}
  rm -rf ${EML_PARSED}
  # Sleep
	sleep ${MAIL_INTERVAL_SECONDS};
done
#
