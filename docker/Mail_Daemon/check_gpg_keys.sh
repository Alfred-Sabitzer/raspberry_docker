#!/bin/bash
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Check der pgp-Keys"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
create_key () {
    cat <<EOF | gpg --batch --yes --verbose --generate-key
           %echo Generating a basic OpenPGP key
           Key-Type: RSA
           Key-Length: 4096
           Key-Usage: sign
           Subkey-Type: RSA
           Subkey-Length: 4096
           Subkey-Usage: encrypt
           Name-Real: ${MAIL_USER}
           Name-Comment: Temporary Email for ${MAIL_USER} with no passphrase, Expiration will be in 2 Months
           Name-Email: ${MAIL_USER}
           %no-protection
           Expire-Date: 2m
           # Do a commit here, so that we can later print "done" :-)
           %commit
           %echo done
EOF
    echo "Key created"
}

#
chown -R $(whoami) ~/.gnupg/
chmod 600 ~/.gnupg/*
chmod 700 ~/.gnupg
chmod 700 ~/.gnupg/openpgp-revocs.d
chmod 700 ~/.gnupg/private-keys-v1.d

gpg --list-key ${MAIL_USER}
retVal=$?
if [ ${retVal} -ne 0 ]; then
    echo "Key for ${MAIL_USER} not found"
    create_key
fi
# get key-Dates
while read line; do
  #echo "#${line}#"
  case "${line}" in
    *[expires:*)
        expirationdate=${line#*expires: }
        expirationdate=${expirationdate%]*}
      ;;
    uid*)
        # Not interesting
      ;;
    *)
       if [[ -n "${line// /}" ]]; then
          revocateid=${line#     }
       fi
      ;;
  esac
done < <(gpg --list-keys ${MAIL_USER} )
echo "expirationdate: ${expirationdate}"
echo "revocateid: ${revocateid}"

mydate=$(date '+%Y-%m-%d')
if [ "#${mydate}#" \< "#${expirationdate}#" ];
then
  echo "Key for ${MAIL_USER} is valid"
else
  echo "Key for ${MAIL_USER} is Expired"
  # If expired revoke key, delete key
  sed -i 's/:-----BEGIN PGP PUBLIC KEY BLOCK-----/-----BEGIN PGP PUBLIC KEY BLOCK-----/' ~/.gnupg/openpgp-revocs.d/${revocateid}.rev
  gpg --import ~/.gnupg/openpgp-revocs.d/${revocateid}.rev
  gpg --batch --yes --delete-secret-key ${revocateid}
  gpg --batch --yes --delete-key ${revocateid}
  rm ~/.gnupg/openpgp-revocs.d/${revocateid}.rev
  # create key
  create_key
fi

## get key-Dates again
#while read line; do
#  #echo "#${line}#"
#  case "${line}" in
#    *[expires:*)
#        expirationdate=${line#*expires: }
#        expirationdate=${expirationdate%]*}
#      ;;
#    uid*)
#        # Not interesting
#      ;;
#    *)
#       if [[ -n "${line// /}" ]]; then
#          revocateid=${line#     }
#       fi
#      ;;
#  esac
#done < <(gpg --list-keys ${MAIL_USER} )
#echo "expirationdate: ${expirationdate}"
#echo "revocateid: ${revocateid}"
# Update Keyserver hkps://keyserver.ubuntu.com
# gpg --send-keys ${revocateid}

# Now iniitialize muacrypt
muacrypt add-account --use-system-keyring --use-key ${MAIL_USER}
#
echo "############################################################################################"
echo "#"
echo "# Valid key present for ${MAIL_USER}"
echo "#"
echo "############################################################################################"