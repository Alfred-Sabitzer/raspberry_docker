#!/bin/bash
echo "############################################################################################"
echo "#"
echo "# Verarbeiten der Mails"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

ls -d -R -A -l ${EML_PARSED}/* > /tmp/dir.txt

#-rw-r--r--    1 root     root          5566 Dec 31 13:16 /EML_PARSED/mail_1.eml.pgp.eml
#drwxr-xr-x    2 root     root          4096 Dec 31 13:16 /EML_PARSED/mail_1.eml.pgp.eml_attachments

if [[ -f /tmp/mails_to_do.txt ]];
then
  COUNTER=0
  cat /tmp/dir.txt |
  while IFS=' ' read -r att size owner group byte month day time xdir
  do
    if [[ -d "${xdir}" ]];
    then
      # $f is a directory
      echo "Dir: ${xdir}"
      let COUNTER++
      cd "${xdir}/"
      pwd
      header=$(cat header.txt)
      #rm -f *
      echo "${header}" > header.txt
      # Exchange Sender and Recipient
      sed -i "s_To: _Toxxxxxx: _" header.txt
      sed -i "s_From: _To: _" header.txt
      sed -i "s_Toxxxxxx: _From: _" header.txt
      cp ${EML_IN}/mail_${COUNTER}.eml ./mail_${COUNTER}.txt
      # Standard Mail Komponenten
      cp --verbose /mail/* .
      # Check ob Empfänger als Key vorhanden ist
#      if [[ -f "autocrypt.txt" ]];
#      then
#        mv autocrypt.txt body.pgp
#      fi
      MAIL_RECIPIENT=$(cat header.txt | grep -i To: )
      MAIL_RECIPIENT=${MAIL_RECIPIENT#*<}
      MAIL_RECIPIENT=${MAIL_RECIPIENT%>*}
      gpg --list-key ${MAIL_RECIPIENT}
      retVal=$?
      if [ ${retVal} -eq 0 ]; then
          echo "Create Encryption for ${MAIL_USER} and ${MAIL_RECIPIENT}" > body.pgp
      fi
      # Add public key
      gpg --output ${MAIL_USER}.pgp --armor --export ${MAIL_USER}
      # Process Mail
      python /WriteMail.py --idir="${xdir}/" --ofile="${EML_OUT}/mail_${COUNTER}.eml" --pgppw="${MAIL_SECRET_KEY_PASSWORD}"
      cp ${EML_OUT}/mail_${COUNTER}.eml ${EML_OUT}/mail_${COUNTER}_draft.eml
      muacrypt process-outgoing < ${EML_OUT}/mail_${COUNTER}_draft.eml > ${EML_OUT}/mail_${COUNTER}.eml
      touch /tmp/mails_to_send.txt
    fi
  done
fi
