# Mail Dämom

Das ist ein Mail-Bot der Kommandos erkennt, und Ergebnisse zurück sendet. 

# Prinzipieller Aufbau

Das Kommando und seine Parameter sind im Header.
Eventuelle Daten sind dann im Text oder in den Attachments (kommt auf das Kommando an).

# pgp -Verschlüsselung

Der Dämon versucht natürlich alles zu verschlüsseln, und regt prinzipiell einen Austausch von Schlüsseln an.

# Keine Verschlüsselung erkannt

Wenn ein Kommando nicht verschlüsselt ist (z.b. bei der ersten Email), dann sendet der Dämon seinen aktuellen Schlüssel, und eine kurze Anleitung was man damit tun sollte.
