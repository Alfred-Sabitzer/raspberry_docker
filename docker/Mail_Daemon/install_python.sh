#!/bin/bash
echo "############################################################################################"
echo "#"
echo "# Installation Python "
echo "#"
echo "############################################################################################"
shopt -o -s errexit  #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

# Install Software
python3 -m ensurepip
pip3 install pycryptodome
pip3 install muacrypt
#pip3 install autocrypt
pip3 install --no-cache --upgrade pip setuptools

# Install python/pip
#pip3 install --upgrade --use-pep517 -r requirements.txt
rm -rf /var/cache/apk/*

echo "############################################################################################"
echo "#"
echo "# Python erfolgreich installiert "
echo "#"
echo "############################################################################################"