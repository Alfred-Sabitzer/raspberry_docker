#!/bin/bash
############################################################################################
#
# Versenden der Email
#
# https://www.linuxshelltips.com/curl-send-email-linux/
# https://think.unblog.ch/en/how-to-send-mail-use-curl/
# https://www.spamresource.com/2022/04/lets-send-emailwith-curl.html
# https://everything.curl.dev/usingcurl/smtp
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

if [[ -f /tmp/mails_to_send.txt ]];
then
  ls -R -A -l ${EML_OUT}/*.eml > /tmp/eml.txt
  cat /tmp/eml.txt |
  while IFS=' ' read -r att size owner group byte month day time xfile
  do
    if [[ ! -z "${xfile}" ]];
    then
      echo "${xfile}" > /tmp/sendfile.txt
      #sed -i "s,${EML_OUT},${EML_IN},g" /tmp/sendfile.txt
      echo "Datei: ${xfile} Attachment: $(cat /tmp/sendfile.txt)"
      bash ${xfile}.sh $(cat /tmp/sendfile.txt)
    fi
  done
fi