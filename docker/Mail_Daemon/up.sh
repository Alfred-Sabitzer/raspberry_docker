#!/bin/bash
# Build the image and start it.
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

image="echomail"
tag=$(date '+%Y%m%d')
docker build --no-cache --pull --force-rm --tag ${image}:${tag}  . -f dockerfile
docker kill ${image}
docker container rm --force ${image}
#sudo chown -R 10001:10001 ./.gnupg
docker run --detach \
  -v ./.gnupg:/home/curlmail/.gnupg \
  --env-file ./echomail.config \
  --name ${image} \
  --restart always ${image}:${tag}
docker ps
docker export "$(docker ps | grep -i ${image}:${tag} | grep -i ${image} | awk '{print $1 }')" | gzip >  ${image}_${tag}.tar.gz
