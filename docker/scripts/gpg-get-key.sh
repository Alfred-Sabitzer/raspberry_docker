#!/usr/bin/env bash
#
# download a public key from a keyserver and write it ascii-armored to a file
#

KEYSERVER="keyserver.ubuntu.com"
GPGOPTS="--batch --quiet"

if [[ ${#} -eq 0 ]]; then
  echo "Usage: ${0} <keyid>"
  exit 1
fi

gpg ${GPGOPTS} --keyserver ${KEYSERVER} --recv ${1}

if [[ ${?} -ne 0 ]]; then
  echo "key '${1}' not found"
  exit 1
fi

gpg ${GPGOPTS} --export --armor -o ${1}.asc ${1}

if [[ -e ${1}.asc ]]; then
  echo "successfully exported the key to '${1}.asc'"
fi