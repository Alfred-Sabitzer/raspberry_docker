#!/bin/bash
############################################################################################
# Lesen des Expiration-Dates
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Eliminate Whitespace
#
trim() {
    local orig="$1"
    local trmd=""
    while true;
    do
        trmd="${orig#[[:space:]]}"
        trmd="${trmd%[[:space:]]}"
        test "$trmd" = "$orig" && break
        orig="$trmd"
    done
    printf -- '%s\n' "$trmd"
}

MAIL_USER=$1
expirationdate="never"
# get key-Dates
while read line; do
  #echo "#${line}#"
  case "${line}" in
    *[*:*)
        expirationdate=${line#*[*:}
        expirationdate=${expirationdate%]*}
      ;;
    uid*)
        # Not interesting
      ;;
    *)
        # Not interesting
      ;;
  esac
done < <(gpg --list-keys ${MAIL_USER} )
echo "$(trim ${expirationdate})"