#!/bin/bash
############################################################################################
# Erzeugen des Autocrypt-keys
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER=$1
Headline="Autocrypt: addr=${MAIL_USER}; prefer-encrypt=mutual; keydata="
Keyline=$(gpg --export --armor ${MAIL_USER} | head -n -2 | tail -n +3 | sed 's/^/ /' )
echo "${Headline}"
echo "${Keyline}"