#!/bin/bash
############################################################################################
# Erzeugen einer Message-ID
############################################################################################
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Message-ID: <171612201283.1056262.16234031438205175183@slainte.at>>
echo "Message-ID: <${RANDOM}.${RANDOM}.${RANDOM}@slainte.at>>"
