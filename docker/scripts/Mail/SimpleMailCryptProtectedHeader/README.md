# Einfache verschlüsselte Mail mit protected Headern

https://www.ietf.org/archive/id/draft-autocrypt-lamps-protected-headers-02.html
Protected Headers sind gut, um z.b. das Subject zu verschlüsseln.

![img.png](img.png)

Wird problemlos im Thunderbird verarbeitet.
