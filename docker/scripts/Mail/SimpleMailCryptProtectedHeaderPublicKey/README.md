# Einfache verschlüsselte Mail mit protected Headern und eingebettetem Public Key

https://www.ietf.org/archive/id/draft-autocrypt-lamps-protected-headers-02.html
Protected Headers sind gut, um z.b. das Subject zu verschlüsseln.

![img.png](img.png)

Nun ist auch der Public-Key im der verschlüsselten Payload.
Wird problemlos im Thunderbird verarbeitet.
Das ist zwar kein praktisches Beispiel (denn ohne Public Key kann eh nicht entschlüsselt werden).

