#!/bin/bash
#
# Generate plain eml
#
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER='test@slainte.at'

message_id=$(../scripts/messageid.sh)
maildate=$(../scripts/date.sh)
cat <<EOF > header.txt
MIME-Version: 1.0
User-Agent: ${0}
Content-Language: en-US, de-AT-frami
From: ${MAIL_USER}
To: Lieber Alfred <alfred@slainte.at>
${message_id}
${maildate}
Subject:  Das ist eine einfache Mail
EOF

boundary_multipart=$(../scripts/boundary.sh)
cat <<EOF > multipart.txt
Content-Type: multipart/mixed; boundary="${boundary_multipart}"
MIME-Version: 1.0

--${boundary_multipart}
Content-Type: text/plain; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit

Hi

Das ist der Inhalt der Plain-Mail.

Diese Email ist einfach nur plain (ohne Attachments usw.).

Have FUN

--${boundary_multipart}--
EOF

cat header.txt multipart.txt > SimpleMail.eml
