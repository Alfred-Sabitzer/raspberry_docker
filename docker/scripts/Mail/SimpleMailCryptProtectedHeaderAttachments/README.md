# Verschlüsselte Mail mit protected Headern und vielen Attachments

https://www.ietf.org/archive/id/draft-autocrypt-lamps-protected-headers-02.html
Protected Headers sind gut, um z.b. das Subject zu verschlüsseln.

![img.png](img.png)

Diese Mail hat viele Attachments (alles im Verzeichnis Attachments).
Das wird dann verschlüsselt und versandt.
Wird problemlos im Thunderbird verarbeitet.

