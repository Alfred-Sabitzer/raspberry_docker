#!/bin/bash
#
# Generate Encrypted plain eml with protected Header
#
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER='test@slainte.at'

# Schlüssel löschen
#../create_keys.sh ${MAIL_USER}
#gpg --export --armor --batch --yes --output ${MAIL_USER}.asc ${MAIL_USER}

message_id=$(../scripts/messageid.sh)
maildate=$(../scripts/date.sh)
cat <<EOF > header.txt
MIME-Version: 1.0
User-Agent: ${0}
Content-Language: en-US, de-AT-frami
From: ${MAIL_USER}
To: Lieber Alfred <alfred@slainte.at>
${message_id}
${maildate}
Subject: ...
$(../scripts/key_get_autocrypt.sh ${MAIL_USER})
EOF

boundary_multipart_mixed=$(../scripts/boundary.sh)
boundary_mixed_multipart=$(../scripts/boundary.sh)

cat <<EOF > multipart.txt
Content-Type: multipart/mixed; boundary="${boundary_multipart_mixed}";
 protected-headers="v1"
MIME-Version: 1.0
From: ${MAIL_USER}
To: Lieber Alfred <alfred@slainte.at>
${message_id}
${maildate}
Subject:  =?UTF-8?Q?$(echo 'Das ist eine verschlüsselte Email mit vielen Attachments 🙈🙉🙊' | iconv --to-code=UTF-8 )?=

--${boundary_multipart_mixed}
Content-Type: multipart/mixed; boundary="${boundary_mixed_multipart}"

--${boundary_mixed_multipart}
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: base64

$(base64 <mail.txt)

EOF

for FILE in ./Attachments/*; do
  cat <<EOF > multipart.part

--${boundary_mixed_multipart}
$(../scripts/file.sh "${FILE}")

$(base64 <"${FILE}")
EOF
  cat multipart.part >> multipart.txt
done;

cat <<EOF >> multipart.txt

--${boundary_mixed_multipart}--

--${boundary_multipart_mixed}--

EOF

echo "" | gpg --batch --yes --passphrase-fd 0 --encrypt --sign --armor --trust-model always --recipient alfred@slainte.at --local-user ${MAIL_USER} --output multipart.txt.pgp ./multipart.txt

boundary_pgp=$(../scripts/boundary.sh)
cat <<EOF > multipart.pgp
Content-Type: multipart/encrypted; protocol="application/pgp-encrypted";
 boundary="${boundary_pgp}"
MIME-Version: 1.0

--${boundary_pgp}
Content-Type: application/pgp-encrypted
MIME-Version: 1.0
Content-Description: PGP/MIME version identification

Version: 1

--${boundary_pgp}
Content-Type: application/octet-stream; name="encrypted.asc"
MIME-Version: 1.0
Content-Description: OpenPGP encrypted message
Content-Disposition: inline; filename="encrypted.asc"

$(cat multipart.txt.pgp)

--${boundary_pgp}--
EOF

cat header.txt multipart.pgp > SimpleMailCryptProtectedHeaderAttachments.eml
