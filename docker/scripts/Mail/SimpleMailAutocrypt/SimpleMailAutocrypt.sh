#!/bin/bash
#
# Generate Encrypted plain eml with protected Header and attach public Key
#
#shopt -o -s errexit #—Terminates the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
MAIL_USER='test@slainte.at'




message_id=$(../scripts/messageid.sh)
maildate=$(../scripts/date.sh)
cat <<EOF > header.txt
MIME-Version: 1.0
User-Agent: ${0}
Content-Language: en-US, de-AT-frami
From: ${MAIL_USER}
To: Lieber Alfred <alfred@slainte.at>
${message_id}
${maildate}
Subject:  =?UTF-8?Q?$(echo 'Im Header befinden sich die Autocrypt Daten' | iconv --to-code=UTF-8 )?=
$(../scripts/key_get_autocrypt.sh ${MAIL_USER})
EOF

boundary_multipart_mixed=$(../scripts/boundary.sh)
boundary_mixed_multipart=$(../scripts/boundary.sh)
cat <<EOF > multipart.txt
Content-Type: multipart/mixed; boundary="${boundary_multipart_mixed}";

--${boundary_multipart_mixed}
Content-Type: multipart/mixed; boundary="${boundary_mixed_multipart}"

--${boundary_mixed_multipart}
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: base64

$(base64 <mail.txt)

--${boundary_mixed_multipart}--

--${boundary_multipart_mixed}--
EOF

cat header.txt multipart.txt > SimpleMailAutocrypt.eml
