# raspberry_docker

Strukturen und Skripten um auf einem lokalen Raspberry Nextcloud und Mailcow zu betreiben.

Beide sind aus dem Internet erreichbar.

# DNS-Server

Es wird davon ausgegangen, dass die richtigen DNS Einträge bereits vorhanden sind. Dazu ist ein externer DNS-Anbieter notwendig z.b. www.noip.com bzw. https://www.duckdns.org/.

# Benutzte Nodeports

| Port  | Service                       |
|-------|-------------------------------|
| 80    | ProxyManager                  |
| 81    | ProxyManager                  |
| 90    | Joomla Webserver              |
| 443   | ProxyManager                  |
| 3478  | Nextcloud Talk                |
| 8000  | Portainer                     |
| 8070  | Joomla Webserver              |
| 8080  | Nextcloud AIO Mastercontainer |
| 9443  | Portainer                     |
| 9001  | Portainer-Agent               |
| 9090  | PhpMyadmin                    |
| 11000 | Nextcloud-aio-apache          |


Diese Ports sind am Node von aussen sichtbar.

