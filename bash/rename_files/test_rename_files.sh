#!/bin/bash
############################################################################################
# Test Rename files
############################################################################################
clear
shopt -o -s errexit	#—Terminates  the shell	script if a	command	returns	an error code.
#shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
shopt -o -s nounset	#-No Variables without definition
shopt -s dotglob # Use shopt -u dotglob to exclude hidden directories
IFS="
"
rm --recursive --dir --force ./Super_Test
mkdir --parents "./Super Test/Noch ein super Test/Ganz ein super Test"
mkdir --parents "./Super Test/Noch ein super Test/Das ist ein leeres Directory"
mkdir --parents "./Super Test/Noch ein super Test/Das ist ein . directory mit Punkt/nomales_dir/dir mit blank"
touch "./Super Test/Noch ein super Test/Ganz ein super Test/und das ist ein file.txt"
touch "./Super Test/Noch ein super Test/Ganz ein super Test/das_ist_ein_Gutes_File.txt"
touch "./Super Test/Noch ein super Test/Ganz ein super Test/Das ist das File mit Accent €.txt"
touch "./Super Test/Noch ein super Test/Das ist ein . directory mit Punkt/nomales_dir/dir mit blank/test.file.test.txt"
./rename_files.sh $(pwd)