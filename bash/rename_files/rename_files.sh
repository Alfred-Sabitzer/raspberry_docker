#!/bin/bash
############################################################################################
# Replace special characters in all filenames
# Filenames will be compliant with piwigo https://de.piwigo.org/
############################################################################################
shopt -o -s errexit	#—Terminates  the shell	script if a	command	returns	an error code.
#shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
shopt -o -s nounset	#-No Variables without definition
shopt -s dotglob # Use shopt -u dotglob to exclude hidden directories
IFS="
"
# Define rename function https://unix.stackexchange.com/questions/216659/how-to-rename-all-files-with-special-characters-and-spaces-in-a-directory
function replace_string()
{
  infile=${1}
  outfileNOSPECIALS=`echo "${infile}"|sed -e 's|[^A-Za-z0-9_öÖüÜäÄß/]|_|g'`
  outfileNOoe=`echo $outfileNOSPECIALS| sed -e 's|ö|oe|g'`
  outfileNOae=`echo $outfileNOoe| sed -e 's|ä|ae|g'`
  outfileNOue=`echo $outfileNOae| sed -e 's|ü|ue|g'`
  outfileNOOE=`echo $outfileNOue| sed -e 's|Ö|OE|g'`
  outfileNOAE=`echo $outfileNOOE| sed -e 's|Ä|AE|g'`
  outfileNOUE=`echo $outfileNOAE| sed -e 's|Ü|UE|g'`
  outfileNOss=`echo $outfileNOUE| sed -e 's|ß|ss|g'`
  echo "${outfileNOss}"
}
# echo $(replace_string 'Das ist ein File mit Sonderzeichen # ; ! § % & ( ) / = ? * > < @ € ö ä ü Ö Ä Ü ß ')

# Loop through all directories recursive
find "${1}" -type d | while IFS= read -r directory; do
  # Loop through files in the target directory
  for file in "${directory}"/*; do
    if [ -f "${file}" ]; then
      # Change only the filename
      xpath=${file%/*}
      xbase=${file##*/}
      xfext=${xbase##*.}
      xpref=${xbase%.*}
      #echo "path='${xpath}', pref='${xpref}', ext='${xfext}'"
      if [ -n ${xpref} ]; then # hidden files with a starting dot
        new_file=$(replace_string "${xpref}")
        new_name="${xpath}/${new_file}.${xfext}"
        if [ "${file}" != "${new_name}" ]; then
          #echo "${file} ${new_name}"
          mv -v ${file} ${new_name}
        fi
      else
        echo "Hidden file: ${directory}"
      fi
    fi
  done
done

# And now rename the directories
xloop="Found"
while [ ${xloop} != "Not Found" ]; do
  xloop="Not Found"
  # Loop through all directories recursive
  while IFS= read -r directory; do
    if [ -d "${directory}" ]; then
      # Change only the last directory
      xpath=${directory%/*}
      xbase=${directory##*/}
      xfext=${xbase##*.}
      xpref=${xbase%.*}
      #echo "${directory} ### ${xbase}"
      if [ -n ${xpref} ]; then # hidden directories with a starting dot
        new_file=$(replace_string "${xbase}")
        new_name="${xpath}/${new_file}"
        if [ "${directory}" != "${new_name}" ]; then
          mv -v ${directory} ${new_name}
          xloop="Found"
        fi
      else
        echo "Hidden Directory: ${directory}"
      fi
    fi
  done <<<$(find "${1}" -type d)
done