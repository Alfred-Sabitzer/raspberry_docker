#!/usr/bin/bash
############################################################################################
# Generieren der Templates
############################################################################################
shopt -o -s	errexit	#—Terminates the shell	script if a	command	returns	an error code.
#shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
shopt -o -s	nounset	#-No Variables without definition
IFS="
"
#template='tf_ods_billing_meta_cloudzone'
template="${1}"
python3 ./xml_parser.py --ifile ../../pentaho-jobs/jobs/${template}.kjb > ./templates/${template}.kjb
python3 ./xml_parser.py --ifile ../../pentaho-jobs/jobs/${template}.ktr > ./templates/${template}.ktr
