#!/bin/bash
# Generieren des Penatho-Jobs und der Transaktionen
# Die Tables sind bereits richtig in der DB angelegt. Dann kann alles generiert werden.
shopt -o -s errexit	#—Terminates the shell	script if a	command	returns	an error code.
#shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
IFS="
"
myTable=${1}
TMPFILE=$(mktemp )
TMPCMD=$(mktemp )
sqlcmd -S SVDWHVIGCISD01.corpnet.at,1433 \
         -U srv_tf_ods_tse \
         -P "${TF_ODS_PASSWORD}" \
         -d TF_ODS -C -R -h-1 -k1 -W -s ";" -Q "SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
                      FROM INFORMATION_SCHEMA.TABLES
                      WHERE TABLE_SCHEMA='ODS' AND TABLE_NAME LIKE '${myTable}%' AND TABLE_TYPE ='BASE TABLE'; " | head -n -2 > ${TMPFILE}

cat <<EOF >> ${TMPCMD}
#!/bin/bash
# Temporäres Shellscript, um die Pentaho-Jobs zu generieren
# Das ist notwendig, um die Verschachtelung in der bash gering zu halten.
#
shopt -o -s errexit	#—Terminates the shell	script if a	command	returns	an error code.
shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
IFS="
"
EOF

cat <<EOF > ./test_all_tf_ods_${myTable}.sh
#!/bin/bash
# Shellscript, um alle Testdaten zu laden
#
shopt -o -s errexit	#—Terminates the shell	script if a	command	returns	an error code.
shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
IFS="
"
EOF

while IFS=";" read -r TABLE_CATALOG TABLE_SCHEMA TABLE_NAME TABLE_TYPE
do
  echo "../Generator/pentahoNG.sh ${TABLE_NAME}" >> ${TMPCMD}
  echo "./tf_ods_${TABLE_NAME}.sh" >> ./test_all_tf_ods_${myTable}.sh
done < ${TMPFILE}
rm -f ${TMPFILE}
chmod 755 ${TMPCMD}
chmod 755 ./test_all_tf_ods_${myTable}.sh
${TMPCMD}
rm -f ${TMPCMD}

