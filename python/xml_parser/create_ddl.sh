#!/bin/bash
# Generieren der DDL-Definition
# Die Tables sind bereits richtig in der DB angelegt. Dann kann alles generiert werden.
shopt -o -s errexit	#—Terminates the shell	script if a	command	returns	an error code.
#shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
IFS="
"
# Überprüfen der Parameter
if [ "${1}" = "" ] ;
then
	echo "Usage: $0 table-name"
	echo "eg: $0 billing_gcp_invoice"
	exit 1
fi
shopt -o -s nounset	#-No Variables without definition
#
TABLE_NAME=${1}
TF_ODS_PREFIX="tf_ods_"
TF_ODS_SCHEMA="ODS"
#
sqlcmd -S SVDWHVIGCISD01.corpnet.at,1433 \
           -U srv_tf_ods_tse \
           -P "${TF_ODS_PASSWORD}" \
           -d TF_ODS -C -R -h-1 -W -s ";" -Q "select ods.getDDL('${TABLE_NAME}') AS 'DDL';" | head -n -2 > ${TF_ODS_PREFIX}${TABLE_NAME}.sql
#