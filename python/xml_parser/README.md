# Pentaho-Generator

Based on a DDL definition, this shell script generates the appropriate job and the simple transformation.

# Systemvoraussetzungen

Die nachfolgenden Links, sind hilfreiche Arbeitsanweisungen

* https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools?view=sql-server-ver16&tabs=ubuntu-install
* https://dba.stackexchange.com/questions/147185/how-to-connect-to-sql-server-using-sqlcmd-on-linux
* https://learn.microsoft.com/de-de/sql/tools/sqlcmd/sqlcmd-use-utility?view=sql-server-ver16
* https://learn.microsoft.com/en-us/sql/connect/odbc/linux-mac/connecting-with-sqlcmd?view=sql-server-ver16
* https://learn.microsoft.com/en-us/sql/relational-databases/system-information-schema-views/system-information-schema-views-transact-sql?view=sql-server-ver16

Es müssen die sqlserver-Tools installiert sein (sqlcmd).

```bash
alfred@alfred-VirtualBox:~$ sqlcmd -?
Microsoft (R) SQL Server Command Line Tool
Version 18.2.0001.1 Linux
Copyright (C) 2017 Microsoft Corporation. All rights reserved.

usage: sqlcmd            [-U login id]          [-P password]
[-S server or Dsn if -D is provided]
[-H hostname]          [-E trusted connection]
[-N Encrypt Connection][-C Trust Server Certificate]
[-d use database name] [-l login timeout]     [-t query timeout]
[-h headers]           [-s colseparator]      [-w screen width]
[-a packetsize]        [-e echo input]        [-I Enable Quoted Identifiers]
[-c cmdend]
[-q "cmdline query"]   [-Q "cmdline query" and exit]
[-m errorlevel]        [-V severitylevel]     [-W remove trailing spaces]
[-u unicode output]    [-r[0|1] msgs to stderr]
[-i inputfile]         [-o outputfile]
[-k[1|2] remove[replace] control characters]
[-y variable length type display width]
[-Y fixed length type display width]
[-p[1] print statistics[colon format]]
[-R use client regional setting]
[-K application intent]
[-M multisubnet failover]
[-b On error batch abort]
[-D Dsn flag, indicate -S is Dsn]
[-X[1] disable commands, startup script, environment variables [and exit]]
[-x disable variable substitution]
[-g enable column encryption]
[-G use Azure Active Directory for authentication]
[-? show syntax summary]
alfred@alfred-VirtualBox:~$ 
```
Ein Demo-Beispiel ist

```bash
sqlcmd -S SVDWHVIGCISD01.corpnet.at,1433 \
         -U srv_tf_ods_tse \
         -P $(cat ${HOME}/.password/TF_ODS_PASSWORD.srv_tf_ods_tse) \
         -d TF_ODS -C -R -h-1 -k1 -W -s ";" -Q "SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, CHARACTER_OCTET_LENGTH, NUMERIC_PRECISION, NUMERIC_PRECISION_RADIX, NUMERIC_SCALE, DATETIME_PRECISION, CHARACTER_SET_CATALOG, CHARACTER_SET_SCHEMA, CHARACTER_SET_NAME, COLLATION_CATALOG, COLLATION_SCHEMA, COLLATION_NAME, DOMAIN_CATALOG, DOMAIN_SCHEMA, DOMAIN_NAME \
         FROM TF_ODS.INFORMATION_SCHEMA.COLUMNS \
         WHERE TABLE_NAME='${table_name}' \
         ORDER BY ORDINAL_POSITION ASC;" | head -n -2 > ${tmpdir}/${table_name}.txt
```
Die Ausgabe erfolgt dann in eine Datei, die man mit einfachen Mitteln parsen kann.

# Aufruf des Generators

Der Aufruf des Generators erfolgt einfach mit dem Namen der Tabelle. Man muß im Arbeitsverzeichnis sein, damit der Output an der richtigen Stelle geschrieben wird.

```bash
./pentahoNG.sh billing_gcp_invoicE
```
Der Parser erzeugt dann die folgenden Dateien:

```
-rwxr-xr-x 1 alfred alfred  334 Okt 27 10:31 ./billing/tf_ods_billing_gcp_invoice.sh
-rw-rw-r-- 1 alfred alfred 1278 Okt 27 10:31 ./billing/tf_ods_billing_gcp_invoice.sql
-rw-rw-r-- 1 alfred alfred 13482 Okt 27 10:31 ./../pentaho-jobs/jobs/tf_ods_billing_gcp_invoice.kjb
-rw-rw-r-- 1 alfred alfred 55174 Okt 27 10:31 ./../pentaho-jobs/jobs/tf_ods_billing_gcp_invoice.ktr
```

Die sql-Datei enthält das generierte DDL-Statement:

````sql
-- TF_ODS.ODS.billing_gcp_invoice definition
-- Drop table
-- DROP TABLE TF_ODS.ODS.billing_gcp_invoice;

CREATE TABLE TF_ODS.ODS.billing_gcp_invoice (
  datum varchar(6) DEFAULT '209901' NOT NULL,
  namedesrechnungskontos varchar(128) DEFAULT 'n.a.' NOT NULL,
  iddesrechnungskontos varchar(32) DEFAULT 'n.a.' NOT NULL,
  projektname varchar(128) DEFAULT 'n.a.' NOT NULL,
  projektid varchar(32) DEFAULT 'ALLGEMEIN' NOT NULL,
  projekthierarchie varchar(128) DEFAULT 'ALLGEMEIN' NOT NULL,
  beschreibungdesdienstes varchar(128) DEFAULT 'n.a.' NOT NULL,
  dienstid varchar(32) DEFAULT 'n.a.' NOT NULL,
  skubeschreibung varchar(128) DEFAULT 'n.a.' NOT NULL,
  skuid varchar(32) DEFAULT 'n.a.' NOT NULL,
  guthabentyp varchar(128) DEFAULT 'n.a.' NOT NULL,
  kostenart varchar(32) DEFAULT 'n.a' NOT NULL,
  startdatumdernutzung date DEFAULT '20990101' NOT NULL,
  enddatumdernutzung date DEFAULT '20990101' NOT NULL,
  nutzungsbetrag float DEFAULT 0 NOT NULL,
  nutzungseinheit varchar(128) DEFAULT 'n.a.' NOT NULL,
  ungerundetekosteneuro float DEFAULT 0 NOT NULL,
  kosteneuro float DEFAULT 0 NOT NULL,
 CONSTRAINT billing_gcp_invoice_pk PRIMARY KEY (datum,dienstid,enddatumdernutzung,iddesrechnungskontos,nutzungsbetrag,nutzungseinheit,projektid,skuid,startdatumdernutzung)
);
````
Das generierte Testfile für die Ladefunktionalität sieht dann so aus:

````bash
#!/bin/bash
#  Aufruf des Penatho-Jobs und der Transaktion
set -x
date
cp ./testdata/billing_gcp_invoice*.csv /tmp/
${KITCHEN} -dir:"${PWD}/../../pentaho-jobs/jobs/"   -level=Basic   -file:"${PWD}/../../pentaho-jobs/jobs/tf_ods_billing_gcp_invoice.kjb"   -param:input_directory="/tmp/" 2>&1 > /tmp/tf_ods_billing_gcp_invoice.log
date
````

Mit diesem bash-Script kann man die Funktionalität des Loaders testen. Alle Ausgabe und Log-Dateien entstehen in /tmp.

Das KJB ist der Pentaho-Job basierend auf dem Template "GeneratorLoadCSVtoTable.kjb"
Das KTR ist die Transformation basierend auf dem Template "GeneratorLoadCSVtoTable.ktr"

# Pentaho-Version

Es wird das Image in "pentaho-image" verwendet. 
Wichtig: man muß relativ zum Verzeichnis stehen.
Der Aufruf erfolgt mit

```bash
./pentaho-image/02_run.sh
```

Im Image wechselt man dann das Verzeinis z.b. nach "billing".
Dann kann man die Testkripten verwenden.

Es gibt auch die Variable $SPOON. Damit wird der graphische Editor von Pentaho aufgerufen.

![pentaho_kjb.png](pics%2Fpentaho_kjb.png)

Das ist der Pentaho-Job. Dieser wird in der Produktion durch das Automic aufgerufen. Als Parameter wird  "-param:input_directory=" benötigt. Dieser Parameter gibt an, wo die zu ladenden Dateien zu finden sind. 

![pentaho_ktr.png](pics%2Fpentaho_ktr.png)

Aus dem Pentaho-Job wird dann die Pentaho-Transaktion aufgerufen. Diese Transaktion liest alle Dateien (passend zur Filemask) und lädt sie in die Datenbenk.
Die einzelnen Schritte sind kommentiert. Die Funktionsweise kann unter https://help.hitachivantara.com/Documentation/Pentaho/Data_Integration_and_Analytics/9.5 nachgelsen werden.

# Methodische Vorgehensweise

## Generieren von Pentaho-Jobs und Pentaho-Transformationen

Das wichtigste ist die Definition in der Datenbank. Die Tabelle braucht einen Unique-Key. Not-Null Values sollten einen Default Wert haben.

Beispiel:

````roomsql
CREATE TABLE TF_ODS.ODS.billing_stage_instances (
  extractdate varchar(6) DEFAULT '202912' NOT NULL,
  stage varchar(32) DEFAULT 'n.a.' NOT NULL,
  name varchar(64) DEFAULT 'n.a' NOT NULL,
  cloudzone varchar(16) DEFAULT 'n.a.' NOT NULL,
  machine_type varchar(16) DEFAULT 'n.a.' NOT NULL,
  preemptible varchar(6) DEFAULT 'n.a.' NOT NULL,
  internal_ip varchar(16) DEFAULT 'n.a.' NOT NULL,
  external_ip varchar(16) DEFAULT 'n.a.' NOT NULL,
  status varchar(16) DEFAULT 'n.a' NOT NULL,
 CONSTRAINT billing_stage_instances_PK PRIMARY KEY (extractdate,name,stage)
);
````
Diese Tabellendefinition entspricht dem CSV-File - Alle Columns sind Spalten im CSV-File.
Die Felder für den Unique-Key müssen "NOT NULL" sein.
Wenn Columns im CSV-File leer (aka NULL) sind, dann werden die Default-Werte übernommen.

Der Generator liest diese Tabellendefinition aus der Datenbank und generiert passend zum gewählten Template den Job und die Transformation.

## Testen der erzeugten Jobs und Transformationen

Der Generator generiert für jede Tabelle ein sql-File (zur Dokumentation) und ein Shell-Skript
````bash
-rwxr-xr-x 1 alfred  546 Okt 31 15:54 test_all_tf_ods_billing.sh
-rwxr-xr-x 1 alfred  334 Okt 31 15:54 tf_ods_billing_gcp_invoice.sh
-rwxr-xr-x 1 alfred  343 Okt 31 15:54 tf_ods_billing_meta_cloudzone.sh
-rwxr-xr-x 1 alfred  334 Okt 31 15:54 tf_ods_billing_meta_config.sh
-rwxr-xr-x 1 alfred  340 Okt 31 15:54 tf_ods_billing_meta_database.sh
-rwxr-xr-x 1 alfred  355 Okt 31 15:54 tf_ods_billing_meta_kostentraeger.sh
-rwxr-xr-x 1 alfred  349 Okt 31 15:54 tf_ods_billing_meta_machinetype.sh
-rwxr-xr-x 1 alfred  355 Okt 31 15:54 tf_ods_billing_meta_teamnamespace.sh
-rwxr-xr-x 1 alfred  361 Okt 31 15:54 tf_ods_billing_stage_databases_size.sh
-rwxr-xr-x 1 alfred  346 Okt 31 15:54 tf_ods_billing_stage_instances.sh
````
Jedes einzelne File lädt das korrespondierende Testfile aus .testdata in die Datenbank.

````bash
cat tf_ods_billing_gcp_invoice.sh
#!/bin/bash
#  Aufruf des Penatho-Jobs und der Transaktion
set -x
date
cp ./testdata/billing_gcp_invoice*.csv /tmp/
${KITCHEN} -dir:"${PWD}/../../pentaho-jobs/jobs/"   -level=Basic   -file:"${PWD}/../../pentaho-jobs/jobs/tf_ods_billing_gcp_invoice.kjb"   -param:input_directory="/tmp/" 2>&1 > /tmp/tf_ods_billing_gcp_invoice.log
date
````

Um alle Testdaten hintereinander laden zu können, ist es eine gute Idee diese Aufrufe in einem separaten Skript zu sammeln, z.b. test_all_tf_ods_billing.sh.

````bash
#!/bin/bash
# Shellscript, um alle Testdaten zu laden
#
shopt -o -s errexit	#—Terminates the shell	script if a	command	returns	an error code.
shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
IFS="
"
./tf_ods_billing_gcp_invoice.sh
./tf_ods_billing_meta_cloudzone.sh
./tf_ods_billing_meta_config.sh
./tf_ods_billing_meta_database.sh
./tf_ods_billing_meta_kostentraeger.sh
./tf_ods_billing_meta_machinetype.sh
./tf_ods_billing_meta_teamnamespace.sh
./tf_ods_billing_stage_databases_size.sh
./tf_ods_billing_stage_instances.sh
````

## Erzeugen von lauffähigen Templates

Um ein Template zu erhalten, braucht man zuerst einen funktionierenden "Hello World"-Job und eine "Hello World"-Transformation.
Das Skript "xml_parser.sh" zeigt, dann wie man daraus ein Template erzeugen kann. Siehe auch "Typen von Transformern".


# Generator Features

## Typen von "Transformern".
https://jira.twinformatics.at/browse/DCCINFRA-10009

Das File xml_parser.sh gib eine Idee, wie aus einem funktionierendem KJB und KTR Templates erzeugt werden können.
Diese Templates können dann für das Generieren von Jobs verwendet werden.

````bash
./xml_parser.sh
````

Es wird der funktionierende Pentaho-Job und die Pentaho-Transformation geparsed. Dabei werden folgene Ersetzungen vorgenommen:

````xml
<connection>#</connection>
<created_date>#</created_date>
<created_user>#</created_user>
<database>#</database>
<description>#</description>
<directory>#</directory>
<enclosure>#</enclosure>
<extended_description>#</extended_description>
<fields>#Constant#</fields>
<fields>#CsvInput#</fields>
<fields>#IfNull#</fields>
<fields>#StringOperations#</fields>
<fields>#WriteToLog#</fields>
<filemask>#</filemask>
<filename>#CsvInput#</filename>
<filename>#TRANS#</filename>
<from>#NAME#</from>
<header>#</header>
<job_status>#</job_status>
<job_version>#</job_version>
<lookup>#InsertUpdate#</lookup>
<modified_date>#</modified_date>
<modified_user>#</modified_user>
<name>#NAME#</name>
<separator>#</separator>
<to>#NAME#</to>
<trans_version>#</trans_version>
````

Im Generator pentahoNG.sh werden diese Platzhalter dann mit aktuellen Werten ersetzt.

## Tabellenspezifische Defaults

Man kann pro Tabelle eine Datei "table.env" spezifizieren. In dieser Datei können die Generator-Defaults überschrieben werden.
Siehe auch https://help.hitachivantara.com/Documentation/Pentaho/Data_Integration_and_Analytics/9.4/Products/CSV_File_Input

Beispiel für mögliche Werte:

````bash
# File for local Settings
# https://help.hitachivantara.com/Documentation/Pentaho/Data_Integration_and_Analytics/9.4/Products/CSV_File_Input
#
#JOBS_DIR="../../pentaho-jobs/jobs"
#TRANSFORMATION_DIR="../../pentaho-jobs/jobs"
#TF_ODS_DATABASE="TF_ODS"
#TF_ODS_SCHEMA="ODS"
#TF_ODS_PREFIX="tf_ods_"
#TEMPLATE_STYLE="./../Generator/templates/tf_ods_billing_meta_cloudzone"
TF_ODS_SEPARATOR=";"
#TF_ODS_DECIMAL=","
#TF_ODS_GROUP="."
#TF_ODS_ENCLOSURE='"'
````
In diesem Beispiel würde der Separator im CSV-File auf ";" gesetzt (Default ist ",").

## getDDL.sql

Diese Datenbankprozedur liefer ein vollständides DDL-File.

```bash
./create_ddl.sh Table_Name
```
Das Ergebnis befindet sich dann im Table_Name.sql

## TODO: Offene Punkte

Es gibt noch viel zu tun.

