CREATE OR ALTER FUNCTION getDDL (@TABIN varchar(100))
    RETURNS varchar(8000)
    WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @TABDDL varchar(8000);
	DECLARE @TABNAME varchar(255);
	DECLARE @TABNAME_FK varchar(255);
	DECLARE @TABNAME_FK_PK varchar(255);
	DECLARE @SchemaName	varchar(255);
	DECLARE @TableName varchar(255);
	DECLARE @Ordinal int;
	DECLARE @ColumnName varchar(255);
	DECLARE @IsNullable varchar(255);
	DECLARE @TypeName varchar(255);
	DECLARE @MaxLength int;
	DECLARE @FPrecision	int;
	DECLARE @FDefault varchar(255);
	DECLARE @DateTimePrecision varchar(255);
	DECLARE @FScale varchar(255);
	DECLARE @IsIdentity varchar(255);
	DECLARE @IsStoreGenerated varchar(255);
	DECLARE @Constraint_Name varchar(255);
	DECLARE @Check_Clause varchar(1024);
	DECLARE @Unique varchar(16);
	DECLARE @IndexName varchar(128);
	DECLARE @Index_Type varchar(16);
	DECLARE @Schema varchar(128);
	DECLARE @Key_Columns varchar(128);
	DECLARE @Included_Columns varchar(128);
    DECLARE @Filter_Definition varchar(128);
	DECLARE @PAD_INDEX varchar(16);
	DECLARE @Statistics_Norecompute varchar(16);
	DECLARE @Fillfactor varchar(16);
	DECLARE @Ignore_Dup_Key varchar(16);
	DECLARE @Allow_Row_Locks varchar(16);
	DECLARE @Allow_Page_Locks varchar(16);
	DECLARE @Extended_Column varchar(1024);
	DECLARE @Extended_Value varchar(1024);
    DECLARE @UPDATE_RULE varchar(16);
    DECLARE @DELETE_RULE varchar(16);

SET @TABNAME=UPPER(@TABIN);

-- Cursor für die Felder
    DECLARE db_cursor CURSOR FOR
    SELECT  c.TABLE_SCHEMA AS SchemaName,
            c.TABLE_NAME AS TableName,
            c.ORDINAL_POSITION AS Ordinal,
            c.COLUMN_NAME AS ColumnName,
            CAST(CASE WHEN IS_NULLABLE = 'YES' THEN 'NULL'
                      ELSE 'NOT NULL'
                 END AS varchar) AS IsNullable,
            DATA_TYPE AS TypeName,
            ISNULL(CHARACTER_MAXIMUM_LENGTH, 0) AS MaxLength,
            CAST(ISNULL(NUMERIC_PRECISION, 0) AS INT) AS FPrecision,
            ISNULL(COLUMN_DEFAULT, '') AS FDefault,
            CAST(ISNULL(DATETIME_PRECISION, 0) AS INT) AS DateTimePrecision,
            ISNULL(NUMERIC_SCALE, 0) AS FScale,
            CAST(COLUMNPROPERTY(OBJECT_ID(QUOTENAME(c.TABLE_SCHEMA) + '.' + QUOTENAME(c.TABLE_NAME)), c.COLUMN_NAME, 'IsIdentity') AS BIT) AS IsIdentity,
            CAST(CASE WHEN COLUMNPROPERTY(OBJECT_ID(QUOTENAME(c.TABLE_SCHEMA) + '.' + QUOTENAME(c.TABLE_NAME)), c.COLUMN_NAME, 'IsIdentity') = 1 THEN 1
                      WHEN COLUMNPROPERTY(OBJECT_ID(QUOTENAME(c.TABLE_SCHEMA) + '.' + QUOTENAME(c.TABLE_NAME)), c.COLUMN_NAME, 'IsComputed') = 1 THEN 1
                      WHEN DATA_TYPE = 'TIMESTAMP' THEN 1
                      ELSE 0
                 END AS BIT) AS IsStoreGenerated
    FROM    INFORMATION_SCHEMA.COLUMNS c
    WHERE UPPER(c.TABLE_NAME)=@TABNAME
    order by c.ORDINAL_POSITION ;

-- Cursor für den Primary Key
    DECLARE pk_cursor CURSOR FOR
   	SELECT  kcu.COLUMN_NAME as ColumnName,
   	        kcu.CONSTRAINT_NAME as Constraint_Name,
   	        kcu.ORDINAL_POSITION as Ordinal
    FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
    WHERE UPPER(kcu.TABLE_NAME)=@TABNAME
        AND CONSTRAINT_NAME=(SELECT CONSTRAINT_NAME
                                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
                                WHERE tc.TABLE_NAME=kcu.TABLE_NAME
                                AND CONSTRAINT_TYPE='PRIMARY KEY')
    ORDER BY ORDINAL_POSITION ASC;

-- Cursor für die Check-Constraints
    DECLARE check_cursor CURSOR FOR
	SELECT	cc.check_clause Check_Clause,
			cc.CONSTRAINT_NAME Constraint_Name,
			ccu.COLUMN_NAME as ColumnName
    FROM    INFORMATION_SCHEMA.CHECK_CONSTRAINTS cc,
            INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
    WHERE   UPPER(ccu.TABLE_NAME)=@TABNAME
        AND     cc.CONSTRAINT_NAME=(SELECT CONSTRAINT_NAME
                                       FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
                                       WHERE tc.TABLE_NAME=ccu.TABLE_NAME
                                           AND CONSTRAINT_TYPE='CHECK')
        AND ccu.CONSTRAINT_NAME = cc.CONSTRAINT_NAME;

-- Cursor für die Indizes
    DECLARE indizes_cursor CURSOR FOR
	SELECT I.name as IndexName,
        CASE WHEN I.is_unique = 1 THEN 'UNIQUE' ELSE ' ' END as 'Unique',
        I.type_desc COLLATE DATABASE_DEFAULT as Index_Type,
        '[' + SCHEMA_NAME(T.schema_id) + ']' as 'Schema',
        '[' + T.name + ']' as TableName,
        STUFF((SELECT ', [' + C.name + CASE WHEN IC.is_descending_key = 0 THEN '] ASC' ELSE '] DESC' END
            FROM sys.index_columns IC INNER JOIN sys.columns C ON  IC.object_id = C.object_id  AND IC.column_id = C.column_id
            WHERE IC.is_included_column = 0 AND IC.object_id = I.object_id AND IC.index_id = I.Index_id
            FOR XML PATH('')), 1, 2, '') as Key_Columns,
        Included_Columns,
        I.filter_definition as Filter_Definition,
        CASE WHEN I.is_padded = 1 THEN 'ON' ELSE 'OFF' END as PAD_INDEX,
        CASE WHEN ST.no_recompute = 0 THEN 'OFF' ELSE 'ON' END as [Statistics_Norecompute],
        CONVERT(VARCHAR(5), CASE WHEN I.fill_factor = 0 THEN 100 ELSE I.fill_factor END) as [Fillfactor],
        CASE WHEN I.ignore_dup_key = 1 THEN 'ON' ELSE 'OFF' END as [Ignore_Dup_Key],
        CASE WHEN I.allow_row_locks = 1 THEN 'ON' ELSE 'OFF' END as [Allow_Row_Locks],
        CASE WHEN I.allow_page_locks = 1 THEN 'ON' ELSE 'OFF' END [Allow_Page_Locks]
    FROM    sys.indexes I INNER JOIN
            sys.tables T ON  T.object_id = I.object_id INNER JOIN
            sys.stats ST ON  ST.object_id = I.object_id AND ST.stats_id = I.index_id INNER JOIN
            sys.data_spaces DS ON  I.data_space_id = DS.data_space_id INNER JOIN
            sys.filegroups FG ON  I.data_space_id = FG.data_space_id LEFT OUTER JOIN
            (SELECT * FROM
                (SELECT IC2.object_id, IC2.index_id,
                    STUFF((SELECT ', ' + C.name FROM sys.index_columns IC1 INNER JOIN
                        sys.columns C ON C.object_id = IC1.object_id
                            AND C.column_id = IC1.column_id
                            AND IC1.is_included_column = 1
                        WHERE  IC1.object_id = IC2.object_id AND IC1.index_id = IC2.index_id
                        GROUP BY IC1.object_id, C.name, index_id  FOR XML PATH('')
                    ), 1, 2, '') as Included_Columns
                FROM sys.index_columns IC2
                GROUP BY IC2.object_id, IC2.index_id) tmp1
                WHERE Included_Columns IS NOT NULL
            ) tmp2
            ON tmp2.object_id = I.object_id AND tmp2.index_id = I.index_id
    WHERE I.is_primary_key = 0 AND I.is_unique_constraint = 0
    AND   UPPER(T.name)=@TABNAME;

-- Cursor für die Extended Properties - Table
    DECLARE ep_table_cursor CURSOR FOR
	SELECT TRIM(CONVERT(varchar(128), value)) AS EP_VALUE
	FROM fn_listextendedproperty (NULL, 'schema', 'ODS', 'table', @TABNAME, NULL, NULL);

-- Cursor für die Extended Properties - Columns
    DECLARE ep_columns_cursor CURSOR FOR
	SELECT TRIM(CONVERT(varchar(128), objname)) AS EP_OBJECT, TRIM(CONVERT(varchar(128), value)) AS EP_VALUE
    FROM fn_listextendedproperty (NULL, 'schema', 'ODS', 'table', @TABNAME, 'COLUMN', NULL);

-- Cursor für die Foreign Key Constraints
    DECLARE fk_cursor CURSOR FOR
    SELECT constraint_name FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE
    WHERE TABLE_NAME=@TABNAME
    AND CONSTRAINT_NAME IN (SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS);


--
-- Start der Verarbeitung
--
	SET @TABDDL=CONCAT('-- ',@TABNAME,' definition
-- Drop table
-- DROP TABLE ',@TABNAME,';

CREATE TABLE ',@TABNAME,' (
');

-- Verarbeiten der Felder
    OPEN db_cursor
    FETCH NEXT FROM db_cursor INTO @SchemaName, @TableName, @Ordinal, @ColumnName, @IsNullable, @TypeName, @MaxLength, @FPrecision, @FDefault, @DateTimePrecision, @FScale, @IsIdentity, @IsStoreGenerated

    WHILE @@FETCH_STATUS = 0
    BEGIN
	      IF (@Ordinal > 1)
	 	  	  SET @TABDDL = @TABDDL +',
';
          SET @TABDDL = @TABDDL + '	'+@ColumnName + ' ' + @TypeName;
          IF (@TypeName = 'varchar')
    			SET @TABDDL = @TABDDL + CONCAT('(',@MaxLength,')');
		  SET @TABDDL = @TABDDL +' '+@IsNullable;
          IF (@FDefault <> '')
    			SET @TABDDL = @TABDDL +' DEFAULT '+@FDefault;
          FETCH NEXT FROM db_cursor INTO  @SchemaName, @TableName,@Ordinal, @ColumnName, @IsNullable, @TypeName, @MaxLength, @FPrecision, @FDefault, @DateTimePrecision, @FScale, @IsIdentity, @IsStoreGenerated
    END
    CLOSE db_cursor
    DEALLOCATE db_cursor

    SET @TABDDL = @TABDDL +'
);

';

-- Verarbeiten der Felder im Primary Key
    OPEN pk_cursor
    FETCH NEXT FROM pk_cursor INTO @ColumnName, @Constraint_Name, @Ordinal

    SET @TABDDL = @TABDDL +'ALTER TABLE '+@TABNAME+'
	ADD CONSTRAINT '+@Constraint_Name+' PRIMARY KEY CLUSTERED (';

    WHILE @@FETCH_STATUS = 0
    BEGIN
       IF (@Ordinal > 1)
            SET @TABDDL = @TABDDL +',';
        SET @TABDDL = @TABDDL +@ColumnName;
        FETCH NEXT FROM pk_cursor INTO  @ColumnName, @Constraint_Name, @Ordinal
    END
    CLOSE pk_cursor
    DEALLOCATE pk_cursor
	SET @TABDDL = @TABDDL +');
';

-- Verarbeiten der Felder für die Check-Constraints
    OPEN check_cursor
    FETCH NEXT FROM check_cursor INTO @Check_Clause, @Constraint_Name, @ColumnName

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @TABDDL = @TABDDL+'ALTER TABLE '+@TABNAME+' WITH NOCHECK ADD CONSTRAINT '+@Constraint_Name+'
	CHECK '+@Check_Clause+';
';
        FETCH NEXT FROM check_cursor INTO @Check_Clause, @Constraint_Name, @ColumnName
    END
    CLOSE check_cursor
    DEALLOCATE check_cursor

-- Verarbeiten der Felder für die Indizes
    SET @TABDDL=@TABDDL+'
-- Indizes
';
    OPEN indizes_cursor
    FETCH NEXT FROM indizes_cursor INTO @IndexName, @Unique, @Index_Type, @Schema, @TableName, @Key_Columns, @Included_Columns, @Filter_Definition, @PAD_INDEX, @Statistics_Norecompute, @Fillfactor, @Ignore_Dup_Key, @Allow_Row_Locks, @Allow_Page_Locks

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @TABDDL = @TABDDL+'CREATE '+@Unique+' '+@Index_Type+' INDEX '+@IndexName+' ON '+@TABNAME+' ('+@Key_Columns+')
	WITH (	PAD_INDEX = '+@PAD_INDEX+', FILLFACTOR = '+@Fillfactor+', IGNORE_DUP_KEY = '+@Ignore_Dup_Key+', STATISTICS_NORECOMPUTE = '+@Statistics_Norecompute+', ALLOW_ROW_LOCKS = '+@Allow_Row_Locks+', ALLOW_PAGE_LOCKS = '+@Allow_Page_Locks+')
	ON [PRIMARY ] ;
';
	    FETCH NEXT FROM indizes_cursor INTO @IndexName, @Unique, @Index_Type, @Schema, @TableName, @Key_Columns, @Included_Columns, @Filter_Definition, @PAD_INDEX, @Statistics_Norecompute, @Fillfactor, @Ignore_Dup_Key, @Allow_Row_Locks, @Allow_Page_Locks
    END
    CLOSE indizes_cursor
    DEALLOCATE indizes_cursor

-- Verarbeiten der Extended Properties für die Tabelle
    SET @TABDDL=@TABDDL+'
-- Extended Table-Properties
EXEC sys.sp_dropextendedproperty @name = N''MS_Description'', @level0type=N''Schema'', @level0name=N''ODS'', @level1type =N''Table'', @level1name = N'''+@TABNAME+''';
';
    OPEN ep_table_cursor
    FETCH NEXT FROM ep_table_cursor INTO @Extended_Value

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @TABDDL = @TABDDL+
            'EXEC sys.sp_addextendedproperty @name=N''MS_Description'', @value=N'''+TRIM(@Extended_Value)+''', @level0type=N''Schema'', @level0name=N''ODS'', @level1type=N''Table'', @level1name=N'''+@TABNAME+''';
';
        FETCH NEXT FROM ep_table_cursor INTO @Extended_Value
    END
    CLOSE ep_table_cursor
    DEALLOCATE ep_table_cursor

-- Verarbeiten der Extended Properties für die Columns
    SET @TABDDL=@TABDDL+'
-- Extended Column-Properties
';
    OPEN ep_columns_cursor
    FETCH NEXT FROM ep_columns_cursor INTO @Extended_Column, @Extended_Value

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @TABDDL = @TABDDL+
            'EXEC sys.sp_addextendedproperty @name=N''MS_Description'', @value=N'''+TRIM(@Extended_Value)+''', @level0type=N''Schema'', @level0name=N''ODS'', @level1type=N''Table'', @level1name=N'''+@TABNAME+', @level2type=N''Column'', @level2name=N'''+@Extended_Column+''';
';

	    FETCH NEXT FROM ep_columns_cursor INTO @Extended_Column, @Extended_Value
    END
    CLOSE ep_columns_cursor
    DEALLOCATE ep_columns_cursor

--- Foreign Keys
    SET @TABDDL=@TABDDL+'
-- Foreign Keys
';
    OPEN fk_cursor
    FETCH NEXT FROM fk_cursor INTO @TABNAME_FK

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @TABDDL = @TABDDL+
            'ALTER TABLE '+@TABNAME+' ADD CONSTRAINT '+@TABNAME_FK+' FOREIGN KEY (';
--ALTER TABLE Sales.TempSalesReason
--   ADD CONSTRAINT FK_TempSales_SalesReason FOREIGN KEY (TempID)
--      REFERENCES Sales.SalesReason (SalesReasonID)
--      ON DELETE CASCADE
--      ON UPDATE CASCADE
--;

--- FK Columns
-- Cursor für die Foreign Key Constraint Columns
            DECLARE fk_columns_cursor CURSOR FOR
            SELECT ordinal_position,column_name FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE TABLE_NAME =@TABNAME AND
                CONSTRAINT_NAME=@TABNAME_FK
            ORDER BY ORDINAL_POSITION ASC;

            OPEN fk_columns_cursor
            FETCH NEXT FROM fk_columns_cursor INTO @Ordinal, @ColumnName

            WHILE @@FETCH_STATUS = 0
            BEGIN
                IF (@Ordinal > 1)
                    SET @TABDDL = @TABDDL +',';
                SET @TABDDL = @TABDDL+@ColumnName;
                FETCH NEXT FROM fk_columns_cursor INTO @Ordinal, @ColumnName
            END
            CLOSE fk_columns_cursor
            DEALLOCATE fk_columns_cursor
        SET @TABDDL = @TABDDL +')';

-- Cursor für den verwendeten Primary Key
            DECLARE fk_pk_cursor CURSOR FOR
            SELECT UNIQUE_CONSTRAINT_NAME,UPDATE_RULE,DELETE_RULE FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
            WHERE CONSTRAINT_NAME =@TABNAME_FK;

            OPEN fk_pk_cursor
            FETCH NEXT FROM fk_pk_cursor INTO @TABNAME_FK_PK, @UPDATE_RULE, @DELETE_RULE

            WHILE @@FETCH_STATUS = 0
            BEGIN
                FETCH NEXT FROM fk_pk_cursor INTO @TABNAME_FK_PK, @UPDATE_RULE, @DELETE_RULE
            END
            CLOSE fk_pk_cursor
            DEALLOCATE fk_pk_cursor

-- Cursor für den verwendeten Primary Key und seine Columns
            DECLARE fk_pk_columns_cursor CURSOR FOR
            SELECT ordinal_position, table_name, column_name from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE CONSTRAINT_NAME=@TABNAME_FK_PK
            ORDER BY ORDINAL_POSITION ASC;

            OPEN fk_pk_columns_cursor
            FETCH NEXT FROM fk_pk_columns_cursor INTO @Ordinal, @TableName, @ColumnName

            WHILE @@FETCH_STATUS = 0
            BEGIN
                IF (@Ordinal = 1)
                    SET @TABDDL = @TABDDL +' REFERENCES '+@TableName+' (';
                IF (@Ordinal > 1)
                    SET @TABDDL = @TABDDL +',';
                SET @TABDDL = @TABDDL+@ColumnName;
                FETCH NEXT FROM fk_pk_columns_cursor INTO @Ordinal, @TableName, @ColumnName
            END
            CLOSE fk_pk_columns_cursor
            DEALLOCATE fk_pk_columns_cursor

        SET @TABDDL = @TABDDL+')  ON DELETE '+@DELETE_RULE+' ON UPDATE '+@UPDATE_RULE+';
';
	    FETCH NEXT FROM fk_cursor INTO @TABNAME_FK
    END
    CLOSE fk_cursor
    DEALLOCATE fk_cursor

--- Ausgabe des fertigen DDL-Statements
    RETURN(@TABDDL);
END;
---
