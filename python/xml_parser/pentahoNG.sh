#!/bin/bash
############################################################################################
# Generate Pentaho Job and Transformation based on DDL-File
############################################################################################
shopt -o -s errexit	#—Terminates  the shell	script if a	command	returns	an error code.
#shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
IFS="
"

# Überprüfen der Parameter
if [ "${1}" = "" ] ;
then
	echo "Usage: $0 table-name"
	echo "eg: $0 billing_gcp_invoice"
	exit 1
fi
shopt -o -s nounset	#-No Variables without definition
table_name=$(echo ${1} | tr '[:upper:]' '[:lower:]')
tmpdir="/tmp"
d=$(date '+%Y/%m/%d %H:%M:%S.%N')

# Umgebungsvarialbe
export JOBS_DIR="../../pentaho-jobs/jobs"
export TRANSFORMATION_DIR="../../pentaho-jobs/jobs"
export TF_ODS_DATABASE="TF_ODS"
export TF_ODS_SCHEMA="ODS"
export TF_ODS_PREFIX="tf_ods_"
export TEMPLATE_STYLE="./../Generator/templates/tf_ods_billing_gcp_invoice"
export TF_ODS_SEPARATOR=","
export TF_ODS_DECIMAL=","
export TF_ODS_GROUP="."
export TF_ODS_ENCLOSURE='"'
mkdir -p ${JOBS_DIR}
mkdir -p ${TRANSFORMATION_DIR}

# Setzen der Default-Variablen
Pname=${table_name}
Pextended_description="Jobs_Dir: ${JOBS_DIR} \nTransformation_Dir: ${TRANSFORMATION_DIR}  \nTF_ODS_SCHEMA: ${TF_ODS_SCHEMA}  \nTF_ODS_PREFIX: ${TF_ODS_PREFIX}  \nTEMPLATE_STYLE: ${TEMPLATE_STYLE}"
Pjob_version="${d}"
Pcreated_user="$0 ${table_name}"
Pcreated_date="${d}"
Pmodified_user="$0 ${table_name}"
Pmodified_date="${d}"
Pfilename="\${Internal.Entry.Current.Directory}/${TF_ODS_PREFIX}${table_name}.ktr"
Pto=${table_name}
Pfrom=${table_name}
Pdescription="Load Data from ${Pfrom} into ${table_name}"
Pdirectory='/'
Penclosure=${TF_ODS_ENCLOSURE}
Pheader='Y'
Pjob_status='2'

# Check ob lokale Variablen Definitionen vorhanden sind
FILE="./${TF_ODS_PREFIX}${table_name}.env"
if [[ -f "${FILE}" ]]; then
    echo "Environment set ${FILE}"
    source ${FILE}
fi

# Vorbereiten der Hilf-Files
rm -f ${tmpdir}/${table_name}.txt
rm -f ${tmpdir}/${table_name}.cpnstant
rm -f ${tmpdir}/${table_name}.fields
rm -f ${tmpdir}/${table_name}.errorfields
rm -f ${tmpdir}/${table_name}.key
rm -f ${tmpdir}/${table_name}.value
rm -f ${tmpdir}/${table_name}.replace
rm -f ${tmpdir}/${table_name}.log
rm -f ${tmpdir}/${table_name}.null

# Lesen der Tabellendefinition
rm -f ${tmpdir}/${table_name}.txt
sqlcmd -S SVDWHVIGCISD01.corpnet.at,1433 \
         -U srv_tf_ods_tse \
         -P "${TF_ODS_PASSWORD}" \
         -d TF_ODS -C -R -h-1 -k1 -W -s ";" -Q "SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, CHARACTER_OCTET_LENGTH, NUMERIC_PRECISION, NUMERIC_PRECISION_RADIX, NUMERIC_SCALE, DATETIME_PRECISION, CHARACTER_SET_CATALOG, CHARACTER_SET_SCHEMA, CHARACTER_SET_NAME, COLLATION_CATALOG, COLLATION_SCHEMA, COLLATION_NAME, DOMAIN_CATALOG, DOMAIN_SCHEMA, DOMAIN_NAME \
         FROM ${TF_ODS_DATABASE}.INFORMATION_SCHEMA.COLUMNS \
         WHERE TABLE_SCHEMA='${TF_ODS_SCHEMA}' AND TABLE_NAME='${table_name}' \
         ORDER BY ORDINAL_POSITION ASC;" | head -n -2 > ${tmpdir}/${table_name}.txt

# Lesen des Primary Keys
rm -f ${tmpdir}/${table_name}.primary
sqlcmd -S SVDWHVIGCISD01.corpnet.at,1433 \
         -U srv_tf_ods_tse \
         -P "${TF_ODS_PASSWORD}" \
         -d TF_ODS -C -R -h-1 -k1 -W -s ";" -Q "select TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, CONSTRAINT_CATALOG, CONSTRAINT_SCHEMA, CONSTRAINT_NAME
                  from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                  where TABLE_NAME='${table_name}'
                        AND CONSTRAINT_NAME=(select CONSTRAINT_NAME
                                            	from INFORMATION_SCHEMA.TABLE_CONSTRAINTS
                                             	where TABLE_SCHEMA='${TF_ODS_SCHEMA}' AND TABLE_NAME='${table_name}'
                                               	     AND CONSTRAINT_TYPE='PRIMARY KEY')
                                              order by ORDINAL_POSITION ASC;" | head -n -2 > ${tmpdir}/${table_name}.primary

# Schreiben des DDL_Files
$(dirname $0)/create_ddl.sh ${table_name}

# Verarbeiten der Tabellendefinition
tabledefinition_empty="Yes"
cat <<EOF > ${tmpdir}/${table_name}.fields
    <fields>
EOF
cat <<EOF > ${tmpdir}/${table_name}.errorfields
    <fields>
EOF
cat <<EOF > ${tmpdir}/${table_name}.replace
    <fields>
EOF
cat <<EOF > ${tmpdir}/${table_name}.log
    <fields>
EOF
cat <<EOF > ${tmpdir}/${table_name}.key
    <lookup>
      <schema>${TF_ODS_SCHEMA}</schema>
      <table>${table_name}</table>
EOF
cat <<EOF > ${tmpdir}/${table_name}.null
    <fields>
EOF

cat <<EOF > ${tmpdir}/${table_name}.constant
    <fields>
      <field>
        <name>filename</name>
        <type>String</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <nullif/>
        <length>-1</length>
        <precision>-1</precision>
        <set_empty_string>N</set_empty_string>
      </field>
      <field>
        <name>wildcard</name>
        <type>String</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <nullif/>
        <length>-1</length>
        <precision>-1</precision>
        <set_empty_string>N</set_empty_string>
      </field>
    </fields>
EOF

while IFS=";" read -r TABLE_CATALOG TABLE_SCHEMA TABLE_NAME COLUMN_NAME ORDINAL_POSITION COLUMN_DEFAULT IS_NULLABLE DATA_TYPE CHARACTER_MAXIMUM_LENGTH CHARACTER_OCTET_LENGTH NUMERIC_PRECISION NUMERIC_PRECISION_RADIX NUMERIC_SCALE DATETIME_PRECISION CHARACTER_SET_CATALOG CHARACTER_SET_SCHEMA CHARACTER_SET_NAME COLLATION_CATALOG COLLATION_SCHEMA COLLATION_NAME DOMAIN_CATALOG DOMAIN_SCHEMA DOMAIN_NAME
do
  #echo ${TABLE_CATALOG},${TABLE_SCHEMA},${TABLE_NAME},${COLUMN_NAME},${ORDINAL_POSITION},${COLUMN_DEFAULT},${IS_NULLABLE},${DATA_TYPE},${CHARACTER_MAXIMUM_LENGTH},${CHARACTER_OCTET_LENGTH},${NUMERIC_PRECISION},${NUMERIC_PRECISION_RADIX},${NUMERIC_SCALE},${DATETIME_PRECISION},${CHARACTER_SET_CATALOG},${CHARACTER_SET_SCHEMA},${CHARACTER_SET_NAME},${COLLATION_CATALOG},${COLLATION_SCHEMA},${COLLATION_NAME},${DOMAIN_CATALOG},${DOMAIN_SCHEMA},${DOMAIN_NAME}
  tabledefinition_empty="No"
  #echo ${DATA_TYPE}
  #echo "-${COLUMN_DEFAULT}-"
  case ${DATA_TYPE} in

    "int")
      cat <<EOF >> ${tmpdir}/${table_name}.fields
      <field>
        <name>${COLUMN_NAME}</name>
        <type>Integer</type>
        <format>#</format>
        <currency/>
        <decimal>,</decimal>
        <group>.</group>
        <length>15</length>
        <precision>0</precision>
        <trim_type>none</trim_type>
      </field>
EOF
      line="  ${COLUMN_NAME} ${DATA_TYPE}"
      if [ "${COLUMN_DEFAULT} " != "NULL " ]; then
        COLUMN_DEFAULT=${COLUMN_DEFAULT:0:-1}
        COLUMN_DEFAULT=${COLUMN_DEFAULT:1}
        line="${line} DEFAULT ${COLUMN_DEFAULT}"
      fi
      if [ "${IS_NULLABLE}" = "NO" ]; then
        line="${line} NOT NULL"
      fi
      line="${line},"
      ;;

    "datetime")
      cat <<EOF >> ${tmpdir}/${table_name}.fields
      <field>
        <name>${COLUMN_NAME}</name>
        <type>Date</type>
        <format>yyyy-MM-dd HH:mm:ss</format>
        <currency/>
        <decimal>,</decimal>
        <group>.</group>
        <length>-1</length>
        <precision>-1</precision>
        <trim_type>none</trim_type>
      </field>
EOF
      line="  ${COLUMN_NAME} ${DATA_TYPE}"
      if [ "${COLUMN_DEFAULT} " != "NULL " ] ;
      then
        COLUMN_DEFAULT=${COLUMN_DEFAULT:0:-1}
        COLUMN_DEFAULT=${COLUMN_DEFAULT:1}
        line="${line} DEFAULT ${COLUMN_DEFAULT}"
      fi
      if [ "${IS_NULLABLE}" = "NO" ]; then
        line="${line} NOT NULL"
      fi
      line="${line},"
      ;;

    "date")
      cat <<EOF >> ${tmpdir}/${table_name}.fields
      <field>
        <name>${COLUMN_NAME}</name>
        <type>Date</type>
        <format>yyyy-MM-dd</format>
        <currency/>
        <decimal>,</decimal>
        <group>.</group>
        <length>-1</length>
        <precision>-1</precision>
        <trim_type>none</trim_type>
      </field>
EOF
      line="  ${COLUMN_NAME} ${DATA_TYPE}"
      if [ "${COLUMN_DEFAULT} " != "NULL " ] ;
      then
        COLUMN_DEFAULT=${COLUMN_DEFAULT:0:-1}
        COLUMN_DEFAULT=${COLUMN_DEFAULT:1}
        line="${line} DEFAULT ${COLUMN_DEFAULT}"
      fi
      if [ "${IS_NULLABLE}" = "NO" ]; then
        line="${line} NOT NULL"
      fi
      line="${line},"
      ;;

    "varchar")
       cat <<EOF >> ${tmpdir}/${table_name}.fields
      <field>
        <name>${COLUMN_NAME}</name>
        <type>String</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <length>-1</length>
        <precision>-1</precision>
        <trim_type>both</trim_type>
      </field>
EOF
       cat <<EOF >> ${tmpdir}/${table_name}.replace
      <field>
        <in_stream_name>${COLUMN_NAME}</in_stream_name>
        <out_stream_name/>
        <trim_type>both</trim_type>
        <lower_upper>none</lower_upper>
        <padding_type>none</padding_type>
        <pad_char/>
        <pad_len/>
        <init_cap>N</init_cap>
        <mask_xml>None</mask_xml>
        <digits>none</digits>
        <remove_special_characters>carriage return  &amp; line feed</remove_special_characters>
      </field>
EOF
      line="  ${COLUMN_NAME} ${DATA_TYPE}(${CHARACTER_MAXIMUM_LENGTH})"
      if [ "${COLUMN_DEFAULT} " != "NULL " ] ;
      then
        COLUMN_DEFAULT=${COLUMN_DEFAULT:0:-1}
        COLUMN_DEFAULT=${COLUMN_DEFAULT:1}
        line="${line} DEFAULT ${COLUMN_DEFAULT}"
      fi
      if [ "${IS_NULLABLE}" = "NO" ]; then
        line="${line} NOT NULL"
      fi
      line="${line},"
      ;;

   "float")
      cat <<EOF >> ${tmpdir}/${table_name}.fields
      <field>
        <name>${COLUMN_NAME}</name>
        <type>Number</type>
        <format/>
        <currency/>
        <decimal>${TF_ODS_DECIMAL}</decimal>
        <group>${TF_ODS_GROUP}</group>
        <length>-1</length>
        <precision>-1</precision>
        <trim_type>none</trim_type>
      </field>
EOF
      line="  ${COLUMN_NAME} ${DATA_TYPE}"
      if [ "${COLUMN_DEFAULT} " != "NULL " ] ;
      then
        COLUMN_DEFAULT=${COLUMN_DEFAULT:0:-2}
        COLUMN_DEFAULT=${COLUMN_DEFAULT:2}
        line="${line} DEFAULT ${COLUMN_DEFAULT}"
      fi
      if [ "${IS_NULLABLE}" = "NO" ]; then
        line="${line} NOT NULL"
      fi
      line="${line},"
      ;;

    *) # Sehr seltsam - Nicht konfigurierter Datentyp
      cat <<EOF >> ${tmpdir}/${table_name}.fields
      <field>
        <name>${COLUMN_NAME}</name>
        <type>${DATA_TYPE}</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <length>-1</length>
        <precision>-1</precision>
        <trim_type>none</trim_type>
      </field>
EOF
      ;;
  esac

    if [ "${COLUMN_DEFAULT} " != "NULL " ] ;
    then # bar=${foo// /.}
      cat <<EOF >> ${tmpdir}/${table_name}.null
      <field>
        <name>${COLUMN_NAME}</name>
        <value>${COLUMN_DEFAULT//\'/}</value>
        <mask/>
        <set_empty_string>N</set_empty_string>
      </field>
EOF
    else
      cat <<EOF >> ${tmpdir}/${table_name}.null
      <field>
        <name>${COLUMN_NAME}</name>
        <value/>
          <mask/>
          <set_empty_string>Y</set_empty_string>
      </field>
EOF
    fi
  cat <<EOF >> ${tmpdir}/${table_name}.value
      <value>
        <name>${COLUMN_NAME}</name>
        <rename>${COLUMN_NAME}</rename>
        <update>Y</update>
      </value>
EOF
  cat <<EOF >> ${tmpdir}/${table_name}.log
      <field>
        <name>${COLUMN_NAME}</name>
      </field>
EOF
#
# Prüfen ob das Feld Teil des Keys ist
#
  if grep -q ";${COLUMN_NAME};" ${tmpdir}/${table_name}.primary
  then
    cat <<EOF >> ${tmpdir}/${table_name}.key
      <key>
        <name>${COLUMN_NAME}</name>
        <field>${COLUMN_NAME}</field>
        <condition>=</condition>
        <name2/>
      </key>
      <value>
        <name>${COLUMN_NAME}</name>
        <rename>${COLUMN_NAME}</rename>
        <update>N</update>
      </value>
EOF
  else
    cat <<EOF >> ${tmpdir}/${table_name}.key
      <value>
        <name>${COLUMN_NAME}</name>
        <rename>${COLUMN_NAME}</rename>
        <update>Y</update>
      </value>
EOF
  fi

done < ${tmpdir}/${table_name}.txt

# Überprüfen ob die Tabelle gültig ist
if [ "${tabledefinition_empty}" = "Yes" ] ;
then
	echo "No Valid Table name found!"
	echo "Usage eg: $0 billing_gcp_invoice"
	exit 1
fi

echo "</lookup>" >> ${tmpdir}/${table_name}.key
cp ${tmpdir}/${table_name}.fields ${tmpdir}/${table_name}.errorfields
cat <<EOF >> ${tmpdir}/${table_name}.errorfields
      <field>
        <name>filename</name>
        <type>String</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <nullif/>
        <trim_type>none</trim_type>
        <length>500</length>
        <precision>-1</precision>
      </field>
      <field>
        <name>NoOfErrorsfieldname</name>
        <type>Integer</type>
        <format>####0;-####0</format>
        <currency/>
        <decimal>.</decimal>
        <group>,</group>
        <nullif/>
        <trim_type>none</trim_type>
        <length>3</length>
        <precision>0</precision>
      </field>
      <field>
        <name>Errordescriptionsfieldname</name>
        <type>String</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <nullif/>
        <trim_type>none</trim_type>
        <length>-1</length>
        <precision>-1</precision>
      </field>
      <field>
        <name>errorsfieldfieldname</name>
        <type>String</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <nullif/>
        <trim_type>none</trim_type>
        <length>-1</length>
        <precision>-1</precision>
      </field>
      <field>
        <name>errorcodesfieldname</name>
        <type>String</type>
        <format/>
        <currency/>
        <decimal/>
        <group/>
        <nullif/>
        <trim_type>none</trim_type>
        <length>-1</length>
        <precision>-1</precision>
      </field>
    </fields>
EOF

echo "</fields>" >> ${tmpdir}/${table_name}.fields
echo "</fields>" >> ${tmpdir}/${table_name}.replace
echo "</fields>" >> ${tmpdir}/${table_name}.null

cat <<EOF >> ${tmpdir}/${table_name}.log
      <field>
        <name>filename</name>
      </field>
      <field>
        <name>row</name>
      </field>
    </fields>
EOF

constraintdefinition_empty="Yes"
line=""
while IFS=";" read -r TABLE_CATALOG TABLE_SCHEMA TABLE_NAME COLUMN_NAME CONSTRAINT_CATALOG CONSTRAINT_SCHEMA CONSTRAINT_NAME
do
  #echo ${TABLE_CATALOG},${TABLE_SCHEMA},${TABLE_NAME},${COLUMN_NAME},${CONSTRAINT_CATALOG},${CONSTRAINT_SCHEMA},${CONSTRAINT_NAME}
  constraintdefinition_empty="No"
  pkline=" CONSTRAINT ${CONSTRAINT_NAME} PRIMARY KEY ("
  if [ "${line} " != " " ] ;
  then
    line="${line},"
  fi
  line="${line}${COLUMN_NAME}"
done < ${tmpdir}/${table_name}.primary

line="${pkline}${line})"

# Überprüfen ob es einen Constraint gibt
if [ "${constraintdefinition_empty}" = "Yes" ] ;
then
	echo "No Valid Constraint for Table ${table_name} found!"
	echo "Please check your Tabledefinition!"
	exit 1
fi

# Kopieren des richtigen Templates
cp ${TEMPLATE_STYLE}.kjb ${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.kjb
cp ${TEMPLATE_STYLE}.ktr ${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.ktr
#echo ${Pextended_description} > ${tmpdir}/extended.txt


# $(replace_string ${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.ktr ${tmpdir}/${table_name}.fields "<fields>#</fields>")
function replace_string()
{
  infile=${1}
  addfile=${2}
  searchstring=${3}
  TMPFILE=$(mktemp )
  while IFS='' read -r line
  do
    if [[ "${line}" == *"${searchstring}"* ]]; then
      cat ${addfile} >> ${TMPFILE}
    else
      echo -e "${line}" >> ${TMPFILE}
    fi
  done < ${infile}
  cat ${TMPFILE} > ${infile}
  rm -f ${TMPFILE}
}

# Ersetzen der variablen Teile
function replace_variables()
{
# #NAME#
#<connection>#</connection>
#<created_date>#</created_date>
#<created_user>#</created_user>
#<database>#</database>
#<description>#</description>
#<directory>#</directory>
#<enclosure>#</enclosure>
#<extended_description>#</extended_description>
#<filemask>#</filemask>
#<filename>#CsvInput#</filename>
#<filename>#TRANS#</filename>
#<from>#NAME#</from>
#<header>#</header>
#<job_status>#</job_status>
#<job_version>#</job_version>
#<modified_date>#</modified_date>
#<modified_user>#</modified_user>
#<name>#NAME#</name>
#<separator>#</separator>
#<to>#NAME#</to>
#<trans_version>#</trans_version>

infile=${1}

#shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
sed -i "s,#NAME#,${Pname},g" ${infile}
sed -i "s,<connection>#</connection>,<connection>${TF_ODS_DATABASE}</connection>,g" ${infile}
sed -i "s,<created_date>#</created_date>,<created_date>${Pcreated_date}</created_date>,g" ${infile}
sed -i "s,<created_user>#</created_user>,<created_user>${Pcreated_user}</created_user>,g" ${infile}
sed -i "s,<database>#</database>,<database>${TF_ODS_DATABASE}</database>,g" ${infile}
sed -i "s,<description>#</description>,<description>${Pdescription}</description>,g" ${infile}
sed -i "s,<directory>#</directory>,<directory>${Pdirectory}</directory>,g" ${infile}
sed -i "s,<enclosure>#</enclosure>,<enclosure>${Penclosure}</enclosure>,g" ${infile}
sed -i "s\"<extended_description>#</extended_description>\"<extended_description>${Pextended_description}</extended_description>\"g" ${infile}
sed -i "s,<filemask>#</filemask>,<filemask>(^${table_name}([a-zA-Z0-9-_]+)?.csv\$)</filemask>,g" ${infile}
sed -i "s,<filename>#</filename>,<filename>${Pfilename}</filename>,g" ${infile}
sed -i "s,<filename>#CsvInput#</filename>,<filename>file://${table_name}.csv</filename>,g" ${infile}
sed -i "s,<filename>#TRANS#</filename>,<filename>\${Internal.Entry.Current.Directory}/${TF_ODS_PREFIX}${table_name}.ktr</filename>,g" ${infile}
sed -i "s,<header>#</header>,<header>${Pheader}</header>,g" ${infile}
sed -i "s,<job_status>#</job_status>,<job_status>${Pjob_status}</job_status>,g" ${infile}
sed -i "s,<job_version>#</job_version>,<job_version>${Pjob_version}</job_version>,g" ${infile}
sed -i "s,<modified_date>#</modified_date>,<modified_date>${Pmodified_date}</modified_date>,g" ${infile}
sed -i "s,<modified_user>#</modified_user>,<modified_user>${Pmodified_user}</modified_user>,g" ${infile}
# Hoffentlich nimmt niemand den & als Separator
sed -i "s&<separator>#</separator>&<separator>${TF_ODS_SEPARATOR}</separator>&g" ${infile}
sed -i "s,<trans_version>#</trans_version>,<trans_version>${Pjob_version}</trans_version>,g" ${infile}

# Das ist für sed ein zu langer String. Darum Prozedural

#<fields>#Constant#</fields>
#<fields>#CsvInput#</fields>
#<fields>#IfNull#</fields>
#<fields>#StringOperations#</fields>
#<fields>#WriteToLog#</fields>
#<fields>#TextFileOutput#</fields>
#<lookup>#InsertUpdate#</lookup>

#sed -i "s,<fields>#</fields>,<fields>$(cat ${tmpdir}/${table_name}.fields)</fields>," ${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.ktr
#sed -i "s,<lookup>#</lookup>,<lookup>$(cat ${tmpdir}/${table_name}.key | tr '\n' '\\n')$(cat ${tmpdir}/${table_name}.value | tr '\n' '\\n')</lookup>,g" ${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.ktr
retval=$(replace_string ${infile} ${tmpdir}/${table_name}.constant "<fields>#Constant#</fields>")
retval=$(replace_string ${infile} ${tmpdir}/${table_name}.fields "<fields>#CsvInput#</fields>")
retval=$(replace_string ${infile} ${tmpdir}/${table_name}.errorfields "<fields>#TextFileOutput#</fields>")
retval=$(replace_string ${infile} ${tmpdir}/${table_name}.null "<fields>#IfNull#</fields>")
retval=$(replace_string ${infile} ${tmpdir}/${table_name}.replace "<fields>#StringOperations#</fields>")
retval=$(replace_string ${infile} ${tmpdir}/${table_name}.log "<fields>#WriteToLog#</fields>")
retval=$(replace_string ${infile} ${tmpdir}/${table_name}.key "<lookup>#InsertUpdate#</lookup>")

}

retval=$(replace_variables ${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.kjb)
retval=$(replace_variables ${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.ktr)

# Schreiben des Testfiles
cat <<EOF > ${TF_ODS_PREFIX}${table_name}.sh
#!/bin/bash
#  Aufruf des Penatho-Jobs und der Transaktion
set -x
date
# Kopieren der Testdaten
cp ./testdata/${table_name}*.csv /tmp/
# Entfernen alter Error- und Logs
rm -f /tmp/${table_name}*.error
rm -f /tmp/${table_name}*.log
# Aufruf Pentaho
\${KITCHEN} -dir:"\${PWD}/${JOBS_DIR}/" \
  -level=Basic \
  -file:"\${PWD}/${JOBS_DIR}/${TF_ODS_PREFIX}${table_name}.kjb" \
  -param:input_directory="/tmp/" 2>&1 > /tmp/${TF_ODS_PREFIX}${table_name}.log
date
EOF
chmod 755 ${TF_ODS_PREFIX}${table_name}.sh

# Hilf-Dateien werden nicht mehr gebraucht.
rm -f ${tmpdir}/${table_name}.txt
rm -f ${tmpdir}/${table_name}.cpnstant
rm -f ${tmpdir}/${table_name}.fields
rm -f ${tmpdir}/${table_name}.errorfields
rm -f ${tmpdir}/${table_name}.primary
rm -f ${tmpdir}/${table_name}.key
rm -f ${tmpdir}/${table_name}.value
rm -f ${tmpdir}/${table_name}.replace
rm -f ${tmpdir}/${table_name}.log
rm -f ${tmpdir}/${table_name}.null
exit
