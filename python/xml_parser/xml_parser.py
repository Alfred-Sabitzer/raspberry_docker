# https://www.geeksforgeeks.org/xml-parsing-python/

import xml.etree.ElementTree as ET
import sys, getopt

rekursion = 0
var_name = ""
ctag = ""
vname = ""
type = ""

def write_to_file(text):
    print(' ' * rekursion + text )

def printxml(item):
    global  rekursion
    global  var_name
    global  type

    rekursion=rekursion+1

    #print(item.tag,'#',item.text)
    if item.tag == 'remove_special_characters' :
        # Bloody Bypass.
        # tree filtert special characters raus.
        item.text='carriage return  &amp; line feed'

    # Check auf NULL-Child
    there_is_a_child='NO'
    for chilld in item :
        there_is_a_child='YES'
        break

    helpvar = item.text
    helpint = 0
    if helpvar == None:
        helpint = 0
    else:
        helpvar = helpvar.strip()
        helpint = len(helpvar)
    if helpint > 0:
# Variablen Ersetzung

        string=item.text
        string=string.replace(var_name,'#NAME#')
        item.text=string

        if item.tag == 'type' :                 # Hier steht immer der zuletzt gelesene Type
            type = item.text
        if item.tag== 'connection' :
            item.text='#'
        if item.tag== 'created_date' :
            item.text='#'
        if item.tag== 'created_user' :
            item.text='#'
        if item.tag== 'database' :
            item.text='#'
        if item.tag== 'description' :
            item.text='#'
        if item.tag== 'directory' :
            item.text='#'
        if item.tag== 'enclosure' :
            item.text='#'
        if item.tag== 'extended_description' :
            item.text='#'
        if item.tag== 'filemask' :
            item.text='#'
        if item.tag== 'filename' :
            item.text='#'+type+'#'
        if item.tag== 'header' :
            item.text='#'
        if item.tag== 'job_status' :
            item.text='#'
        if item.tag== 'job_version' :
            item.text='#'
        if item.tag== 'modified_date' :
            item.text='#'
        if item.tag== 'modified_user' :
            item.text='#'
        if item.tag== 'separator' :
            item.text='#'
        if item.tag== 'trans_version' :
            item.text='#'

        write_to_file('<'+item.tag+'>'+item.text+'</'+item.tag+'>')
    elif there_is_a_child =='YES':
        if item.tag == 'fields' or item.tag == 'lookup' :
            item.text='#'+type+'#'
            write_to_file('<'+item.tag+'>'+item.text+'</'+item.tag+'>')
        else :
            write_to_file('<'+item.tag+'>')
            for child in item:
                printxml(child)
            write_to_file('</'+item.tag+'>')
    else :
        write_to_file('<'+item.tag+'/>')

    rekursion=rekursion-1

def parseXML(inputfile):
    global var_name

# create element tree object
    tree = ET.parse(inputfile)
    write_to_file('<?xml version="1.0" encoding="UTF-8"?>')
    # get root element
    root = tree.getroot()
    write_to_file('<'+root.tag+'>')

    if root.tag == 'job':
        for child in root:
            #print(child.tag, child.attrib)
            if child.tag == 'name' :
                var_name = child.text
                break

    if root.tag == 'transformation':
        for child in root:
            #print(child.tag, child.attrib)
            if child.tag == 'info' :
                for gc in child:
                    #print(child.tag, child.attrib)
                    if gc.tag == 'name' :
                        var_name = gc.text
                        break
                break

    # iterate news items
    for item in root.findall('*'):
        # Adding a subtag
        printxml(item)

    write_to_file('</'+root.tag+'>')

def main(argv):
    # Parameter
    inputfile = ''
    opts, args = getopt.getopt(argv,"hi:o:",["ifile="])
    for opt, arg in opts:
        if opt == '-h':
            print ('xml_parser.py -i <inputfile>')
            print ('xml_parser.py --ifile <inputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg

    if (inputfile  == '' ) :
        print ('xml_parser.py -i <inputfile> ')
        print ('xml_parser.py --ifile <inputfile>')
        sys.exit()

    # parse xml file
    parseXML(inputfile)

if __name__ == "__main__":
    # calling main function
    main(sys.argv[1:])
